# <a href="https://tripetto.com/"><img src="https://unpkg.com/tripetto/assets/banner.svg" alt="Tripetto"></a>

Tripetto is a full-fledged form kit. Rapidly build and run smart flowing forms and surveys. Drop the kit in your codebase and use all of it or just the parts you need. The visual [**builder**](https://www.npmjs.com/package/tripetto) is for form building, and the [**runners**](https://www.npmjs.com/package/tripetto-runner-foundation) are for running those forms in different UI variants. It is entirely extendible and customizable. Anyone can build their own building [**blocks**](https://docs.tripetto.com/guide/blocks) (e.g., question types) or runner UI's.

# Calculator block
[![Status](https://gitlab.com/tripetto/blocks/calculator/badges/master/pipeline.svg)](https://gitlab.com/tripetto/blocks/calculator/commits/master)
[![Version](https://img.shields.io/npm/v/tripetto-block-calculator.svg)](https://www.npmjs.com/package/tripetto-block-calculator)
[![License](https://img.shields.io/npm/l/tripetto-block-calculator.svg)](https://opensource.org/licenses/MIT)
[![Downloads](https://img.shields.io/npm/dt/tripetto-block-calculator.svg)](https://www.npmjs.com/package/tripetto-block-calculator)
[![Follow us on Twitter](https://img.shields.io/twitter/follow/tripetto.svg?style=social&label=Follow)](https://twitter.com/tripetto)

Calculator block for Tripetto. This block can perform all sorts of calculations (see a list of supported operations below). It is a headless block, so it doesn't require a UI implementation for the runner.

## Supported operations

| Operation | Description |
| --- | --- |
| **INPUTS** | |
| Static number | Supplies a static number to the calculator. |
| Recall value | Supplies the value of another question block (for example from a number block, rating block or scale block) to the calculator. |
| Subcalculation | Performs a (sub) calculation for multistep formulas and supplies the result to the calculator. |
| **FUNCTIONS** | |
| *Limiting* | |
| min | Returns the input with the lowest number. |
| max | Returns the input with the highest number. |
| clamp | Clamps (restricts) the input between the specified minimum and maximum value ([learn more](https://en.wikipedia.org/wiki/Clamping_%28graphics%29)). |
| *Floating point* | |
| round | Rounds a floating point number ([learn more](https://en.wikipedia.org/wiki/Rounding)). |
| floor | Rounds a floating point number down ([learn more](https://en.wikipedia.org/wiki/Floor_and_ceiling_functions)). |
| ceil | Rounds a floating point number up ([learn more](https://en.wikipedia.org/wiki/Floor_and_ceiling_functions)). |
| trunc | Removes decimals from a floating point number. |
| *Exponentiation* | |
| x² | Multiplies the given input by itself ([learn more](https://en.wikipedia.org/wiki/Square_%28algebra%29)). |
| √ | Finds the principal square root for the given input ([learn more](https://en.wikipedia.org/wiki/Square_root)). |
| pow | Calculates the base to the power of the given exponent ([learn more](https://en.wikipedia.org/wiki/Exponentiation)). |
| exp | Calculates `e` to the power of the given exponent ([learn more](https://en.wikipedia.org/wiki/Exponential_function)). |
| ln | Calculates the natural logarithm of the input ([learn more](https://en.wikipedia.org/wiki/Natural_logarithm)). |
| log | Calculates the base 10 logarithm of the input ([learn more](https://en.wikipedia.org/wiki/Logarithm)). |
| *Trigonometry* | |
| sin | Calculates the sine of the given angle ([learn more](https://en.wikipedia.org/wiki/Trigonometric_functions)). |
| cos | Calculates the cosine of the given angle ([learn more](https://en.wikipedia.org/wiki/Trigonometric_functions)). |
| tan | Calculates the tangent of the given angle ([learn more](https://en.wikipedia.org/wiki/Trigonometric_functions)). |
| sin⁻¹ | Calculates the inverse sine (arcsine) of the given number ([learn more](https://en.wikipedia.org/wiki/Inverse_trigonometric_functions)). |
| cos⁻¹ | Calculates the inverse cosine (arccosine) of the given number ([learn more](https://en.wikipedia.org/wiki/Inverse_trigonometric_functions)). |
| tan⁻¹ | Calculates the inverse tangent (arctangent) of the given number ([learn more](https://en.wikipedia.org/wiki/Inverse_trigonometric_functions)). |
| sinh | Calculates the hyperbolic sine of the given hyperbolic angle ([learn more](https://en.wikipedia.org/wiki/Hyperbolic_functions)). |
| cosh | Calculates the hyperbolic cosine of the given hyperbolic angle ([learn more](https://en.wikipedia.org/wiki/Hyperbolic_functions)). |
| tanh | Calculates the hyperbolic tangent of the given hyperbolic angle ([learn more](https://en.wikipedia.org/wiki/Hyperbolic_functions)). |
| sinh⁻¹ | Calculates the inverse hyperbolic sine (arcsine) of the given number ([learn more](https://en.wikipedia.org/wiki/Inverse_hyperbolic_functions)). |
| cosh⁻¹ | Calculates the inverse hyperbolic cosine (arccosine) of the given number ([learn more](https://en.wikipedia.org/wiki/Inverse_hyperbolic_functions)). |
| tanh⁻¹ | Calculates the inverse hyperbolic tangent (arctangent) of the given number ([learn more](https://en.wikipedia.org/wiki/Inverse_hyperbolic_functions)). |
| *Factorial* | |
| n! | Calculates the factorial of a positive number ([learn more](https://en.wikipedia.org/wiki/Factorial)). |
| gamma | Calculates the gamma of a positive number ([learn more](https://en.wikipedia.org/wiki/Gamma_function)). |
| *Miscellaneous* | |
| abs | Retrieves the absolute value (or modulus) of a number ([learn more](https://en.wikipedia.org/wiki/Absolute_value)). |
| age | Calculates the age based on the input of a date block, for example a date of birth. |
| year | Calculates the year of a given date. |
| month | Calculates the month of a given date. |
| day of month | Calculates the day of the month of a given date. |
| day of week | Calculates the day of the week of a given date. |
| hour | Calculates the hour of a given time. |
| minute | Calculates the minute of a given time. |
| second | Calculates the second of a given time. |
| millisecond | Calculates the millisecond of a given time. |
| count | Counts the number of selected options, for example from a checkboxes block, or a picture choice block (multiple selection). |
| mod | Calculates the remainder of a division ([learn more](https://en.wikipedia.org/wiki/Modulo_operation)). |
| score | Scores options of a block (for example a dropdown block, radio buttons block or a picture choice block). |
| sgn | Extracts the sign of a number ([learn more](https://en.wikipedia.org/wiki/Sign_function)). |
| % | Calculates a percentage of the input ([learn more](https://en.wikipedia.org/wiki/Percentage)). |
| **COMPARATORS** | |
| Compare value | Compares the recalled value of another block and outputs a value based on the result of the comparison. |
| Compare current outcome | Compares the current outcome of the calculator and outputs a value based on the result of the comparison. |
| Compare number | Compares a static number and outputs a value based on the result of the comparison. |
| Compare date/time | Compares a date (and time) and outputs a value based on the result of the comparison. |
| Check selected option | Checks if a certain option is checked (for example from a multiple choice block) and outputs a value based on the result of the comparison. |
| **TEXT FUNCTIONS** | |
| Character count | Counts the number of characters in a text. |
| Word count | Counts the number of words in a text. |
| Line count | Counts the number of lines in a text. |
| Count occurrences | Counts the number of occurrences of a certain text or character. |
| Convert to number | Converts a text value to a number. |
| **CONSTANTS** | |
| π | Supplies the constant value of `π (pi ≈ 3.14159)` to the calculator ([learn more](https://en.wikipedia.org/wiki/Pi)). |
| e | Supplies the constant value of `e (Euler's constant ≈ 2.71828)` to the calculator ([learn more](https://en.wikipedia.org/wiki/E_%28mathematical_constant%29)). |
| γ | Supplies the constant value of `γ (Euler–Mascheroni constant ≈ 0.57722)` to the calculator ([learn more](https://en.wikipedia.org/wiki/Euler%E2%80%93Mascheroni_constant)). |
| c | Supplies the constant value of `c (speed of light = 299792458 m/s)` to the calculator ([learn more](https://en.wikipedia.org/wiki/Speed_of_light)). |
| Random value | Supplies a random value (0 to less than 1) to the calculator. |
| UNIX time | Supplies the current UNIX time (seconds since Unix Epoch) to the calculator. |
| Year | Supplies the current year to the calculator. |
| Month | Supplies the current month (January = 1) to the calculator. |
| Day of month | Supplies the current day of month (1-31) to the calculator. |
| Day of week | Supplies the current day of week (Sunday = 0) to the calculator. |
| Hour | Supplies the current hour (0-23) to the calculator. |
| Minute | Supplies the current minute (0-59) to the calculator. |
| Second | Supplies the current second (0-59) to the calculator. |
| Millisecond | Supplies the current millisecond (0-999) to the calculator. |
| Timezone | Supplies the current timezone (in milliseconds) to the calculator. |

# Get started
You need to install or import this block to use it in Tripetto. If you are using the CLI version of the builder, you can find instructions [here](https://docs.tripetto.com/guide/builder/#cli-configuration). If you are embedding the builder into your own project using the library, take a look [here](https://docs.tripetto.com/guide/builder/#library-blocks).

# Use cases
- Directly in your browser (add `<script src="https://unpkg.com/tripetto-block-calculator"></script>` to your HTML);
- In the CLI builder (install the block using `npm i tripetto-block-calculator -g` and update your Tripetto [config](https://docs.tripetto.com/guide/builder/#cli-configuration));
- In your builder implementation (just add `import "tripetto-block-calculator";` to your code);
- In your runner implementation (simply add `import "tripetto-block-calculator/runner";` to your code.

# Support
Run into issues or bugs? Report them [here](https://gitlab.com/tripetto/blocks/calculator/issues) and we'll look into them.

For general support contact us at [support@tripetto.com](mailto:support@tripetto.com). We're more than happy to assist you.

# License
Have a blast. [MIT](https://opensource.org/licenses/MIT).

# Contributors
- [Hisam A Fahri](https://gitlab.com/hisamafahri) (Indonesian translation)

# About us
If you want to learn more about Tripetto or contribute in any way, visit us at [Tripetto.com](https://tripetto.com/).
