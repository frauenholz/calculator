/** Dependencies */
import { isNumberFinite } from "tripetto-runner-foundation";

export function gamma(n: number): number | undefined {
    if (n > 0) {
        if (n >= 0.5) {
            const c = [
                0.99999999999980993, 676.5203681218851, -1259.1392167224028,
                771.32342877765313, -176.61502916214059, 12.507343278686905,
                -0.13857109526572012, 9.9843695780195716e-6,
                1.5056327351493116e-7,
            ];
            let x = c[0];

            n--;

            for (let i = 1; i < 9; i++) {
                x += c[i] / (n + i);
            }

            const t = n + 7.5;

            return (
                Math.sqrt(2 * Math.PI) * Math.pow(t, n + 0.5) * Math.exp(-t) * x
            );
        } else {
            const g = gamma(1 - n);

            return isNumberFinite(g)
                ? Math.PI / (Math.sin(Math.PI * n) * g)
                : undefined;
        }
    }

    return undefined;
}
