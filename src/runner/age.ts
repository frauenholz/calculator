/** Dependencies */
import { Num } from "tripetto-runner-foundation";

export type TAge =
    | "years"
    | "months"
    | "days"
    | "hours"
    | "minutes"
    | "seconds"
    | "milliseconds";

export function calculateAgeInMonths(a: number, b: number): number {
    const from = new Date(Num.min(a, b));
    const to = new Date(Num.max(a, b));
    const years = to.getFullYear() - from.getFullYear();
    const months = to.getMonth() - from.getMonth();
    const days = to.getDate() - from.getDate();

    return (years * 12 + months - (days < 0 ? 1 : 0)) * (a > b ? -1 : 1);
}

export function calculateAgeInYears(a: number, b: number): number {
    const from = new Date(Num.min(a, b));
    const to = new Date(Num.max(a, b));
    const months = to.getMonth() - from.getMonth();
    let years = to.getFullYear() - from.getFullYear();

    if (months < 0 || (months === 0 && to.getDate() < from.getDate())) {
        years--;
    }

    return years * (a > b ? -1 : 1);
}
