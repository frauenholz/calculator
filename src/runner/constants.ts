import { Context } from "tripetto-runner-foundation";

export type TConstants =
    | "π"
    | "e"
    | "γ"
    | "c"
    | "random"
    | "timestamp"
    | "year"
    | "month"
    | "day"
    | "day-of-week"
    | "hour"
    | "minute"
    | "second"
    | "millisecond"
    | "timezone"
    | "branch";
export const CONSTANTS: TConstants[] = [
    "π",
    "e",
    "γ",
    "c",
    "random",
    "timestamp",
    "year",
    "month",
    "day",
    "day-of-week",
    "hour",
    "minute",
    "second",
    "millisecond",
    "timezone",
    "branch",
];

export function isConstant(value: string | number | undefined): boolean {
    if (typeof value === "string") {
        const length = CONSTANTS.length;

        for (let constant = 0; constant < length; constant++) {
            if (CONSTANTS[constant] === value) {
                return true;
            }
        }
    }

    return false;
}

export function castToConstant(value: string | number | undefined): TConstants {
    return typeof value === "string" && isConstant(value)
        ? (value as TConstants)
        : "π";
}

export function getConstant(
    constant: TConstants | string,
    context: Context
): number | undefined {
    switch (constant) {
        case "π":
            return Math.PI;
        case "e":
            return Math.E;
        case "γ":
            return 0.577215664901532860606512090082402431042;
        case "c":
            return 299792458;
        case "random":
            return Math.random();
        case "timestamp":
            return Date.now() / 1000;
        case "year":
            return new Date().getFullYear();
        case "month":
            return new Date().getMonth() + 1;
        case "day":
            return new Date().getDate();
        case "day-of-week":
            return new Date().getDay();
        case "hour":
            return new Date().getHours();
        case "minute":
            return new Date().getMinutes();
        case "second":
            return new Date().getSeconds();
        case "millisecond":
            return new Date().getMilliseconds();
        case "timezone":
            return new Date().getTimezoneOffset() * 60 * 1000;
        case "branch":
            return context.index;
    }

    return undefined;
}
