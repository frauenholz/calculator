export interface IScores {
    readonly [id: string]: number | undefined;
}
