/** Package information retrieved from `package.json` using webpack. */
declare const PACKAGE_NAME: string;

/** Dependencies */
import {
    HeadlessBlock,
    Slots,
    assert,
    tripetto,
} from "tripetto-runner-foundation";
import { calculator } from "./calculator";
import { IOperation } from "./operation";
import "./condition";

/** Exports */
export { calculator } from "./calculator";
export { IOperation } from "./operation";

@tripetto({
    type: "headless",
    identifier: PACKAGE_NAME,
})
export class Calculator extends HeadlessBlock<{
    readonly operations?: IOperation[];
}> {
    readonly calculatorSlot = assert(
        this.valueOf<number, Slots.Numeric>("calculator")
    );

    do(): void {
        calculator(
            this.context,
            this.props.operations || [],
            undefined,
            this.calculatorSlot,
            (id) => this.variableFor(id),
            (id) => this.parseVariables(id),
            (id) => this.valueOf(id, "dynamic")
        );
    }
}
