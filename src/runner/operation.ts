/** Dependencies */
import { TOperators } from "./operators";
import { TOpcodes } from "./opcodes";
import { TModes } from "./modes";
import { TAngles } from "./angles";
import { TAge } from "./age";
import { IScores } from "./scores";

export interface IOperation {
    readonly id: string;
    readonly operator: TOperators;
    readonly opcode: TOpcodes;
    readonly operations?: IOperation[];
    readonly reference?: string | number;
    readonly value?: string | number;
    readonly exponent?: string | number;
    readonly cona?: string | number;
    readonly conb?: string | number;
    readonly outa?: string | number;
    readonly outb?: string | number;
    readonly compareMode?: TModes;
    readonly angleUnits?: TAngles;
    readonly ageIn?: TAge;
    readonly ageAbs?: boolean;
    readonly ignoreCase?: boolean;
    readonly scores?: IScores;
    readonly min?: string | number;
    readonly max?: string | number;
    readonly divisor?: string | number;
    readonly percentage?: string | number;
}
