/** Dependencies */
import { isNumberFinite } from "tripetto-runner-foundation";
import { gamma } from "./gamma";

export function factorial(n: number): number | undefined {
    if (n >= 0) {
        if (n === 0) {
            return 1;
        }

        if (n < 1) {
            return gamma(n + 1);
        }

        let f = 0;

        for (let i = 1; i <= n; i++) {
            if (!isNumberFinite(f)) {
                return undefined;
            }

            f += Math.log(i);
        }

        return Math.exp(f);
    }

    return undefined;
}
