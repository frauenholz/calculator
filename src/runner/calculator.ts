/** Dependencies */
import {
    Context,
    DateTime,
    IVariable,
    Num,
    Slots,
    Str,
    Value,
    arraySize,
    castToFloat,
    each,
    isFilledString,
    isNumberFinite,
    isString,
} from "tripetto-runner-foundation";
import { IOperation } from "./operation";
import { convertAngle, convertToRadians } from "./angles";
import { calculateAgeInMonths, calculateAgeInYears } from "./age";
import { gamma } from "./gamma";
import { factorial } from "./factorial";
import { getConstant } from "./constants";
import { TOperators } from "./operators";

function prepareInput(
    input: string | number | undefined,
    answer: number | undefined,
    context: Context,
    variableFor: (id: string) => IVariable | undefined
): number | undefined {
    if (isString(input)) {
        const constant = getConstant(input, context);

        if (isNumberFinite(constant)) {
            return constant;
        }

        const variable = variableFor(input);

        return variable &&
            variable.hasValue &&
            (variable.slot instanceof Slots.Number ||
                variable.slot instanceof Slots.Numeric ||
                variable.slot instanceof Slots.Date)
            ? variable.slot.toValue(variable.value)
            : undefined;
    }

    return isNumberFinite(input) ? input : answer;
}

function execute(
    input: number | undefined,
    operation: (input: number) => number | undefined
): number | undefined {
    return isNumberFinite(input) ? operation(input) : undefined;
}

function process(
    context: Context,
    operator: TOperators,
    operation: IOperation,
    answer: number | undefined,
    variableFor: (id: string) => IVariable | undefined,
    parseVariables: (id: string) => string,
    getSlot?: (id: string) => Value | undefined
): number | undefined {
    let result: number | undefined = 0;

    switch (operation.opcode) {
        case "abs":
            result = execute(
                prepareInput(operation.value, answer, context, variableFor),
                (input) => Math.abs(input)
            );
            break;
        case "acos":
            result = execute(
                prepareInput(operation.value, answer, context, variableFor),
                (input) => convertAngle(Math.acos(input), operation.angleUnits)
            );
            break;
        case "acosh":
            result = execute(
                prepareInput(operation.value, answer, context, variableFor),
                (input) => Math.acosh(input)
            );
            break;
        case "age":
            result = (() => {
                const input =
                    isFilledString(operation.value) &&
                    variableFor(operation.value);

                if (
                    input &&
                    input.hasValue &&
                    input.slot instanceof Slots.Date
                ) {
                    const reference = prepareInput(
                        operation.reference,
                        DateTime.UTC,
                        context,
                        variableFor
                    );
                    const value = input.slot.toValue(input.value);

                    if (isNumberFinite(reference)) {
                        const i = input.slot.toValue(reference);

                        switch (operation.ageIn) {
                            case "milliseconds":
                                return i - value;
                            case "seconds":
                                return Num.floor((i - value) / 1000);
                            case "minutes":
                                return Num.floor((i - value) / (1000 * 60));
                            case "hours":
                                return Num.floor(
                                    (i - value) / (1000 * 60 * 60)
                                );
                            case "days":
                                return Num.floor(
                                    (i - value) / (1000 * 60 * 60 * 24)
                                );
                            case "months":
                                return calculateAgeInMonths(value, i);
                            default:
                                return calculateAgeInYears(value, i);
                        }
                    }
                }

                return undefined;
            })();

            if (operation.ageAbs && isNumberFinite(result)) {
                result = Math.abs(result);
            }
            break;
        case "asin":
            result = execute(
                prepareInput(operation.value, answer, context, variableFor),
                (input) => convertAngle(Math.asin(input), operation.angleUnits)
            );
            break;
        case "asinh":
            result = execute(
                prepareInput(operation.value, answer, context, variableFor),
                (input) => Math.asinh(input)
            );
            break;
        case "atan":
            result = execute(
                prepareInput(operation.value, answer, context, variableFor),
                (input) => convertAngle(Math.atan(input), operation.angleUnits)
            );
            break;
        case "atanh":
            result = execute(
                prepareInput(operation.value, answer, context, variableFor),
                (input) => Math.atanh(input)
            );
            break;
        case "boolean":
            result = (() => {
                const input =
                    isFilledString(operation.value) &&
                    variableFor(operation.value);

                if (input && input.slot instanceof Slots.Boolean) {
                    return prepareInput(
                        input.hasValue && input.value === true
                            ? operation.outa
                            : operation.outb,
                        answer,
                        context,
                        variableFor
                    );
                }

                return undefined;
            })();

            break;
        case "calc":
            const slot = getSlot && getSlot(operation.id);

            if (operation.operations && operation.operations.length > 0) {
                result = answer;

                each(operation.operations, (subOperation) => {
                    if (isNumberFinite(result)) {
                        result = process(
                            context,
                            subOperation.operator,
                            subOperation,
                            result,
                            variableFor,
                            parseVariables,
                            getSlot
                        );
                    }
                });
            }

            if (slot) {
                slot.set(result);
            }

            break;
        case "cast":
            result = (() => {
                const variable =
                    isFilledString(operation.value) &&
                    variableFor(operation.value);

                return variable && variable.hasValue
                    ? castToFloat(variable.string)
                    : 0;
            })();
            break;
        case "ceil":
            result = execute(
                prepareInput(operation.value, answer, context, variableFor),
                (input) => Num.ceil(input)
            );
            break;
        case "chars":
            result = (() => {
                const variable =
                    isFilledString(operation.value) &&
                    variableFor(operation.value);

                return variable && variable.hasValue
                    ? variable.string.length
                    : 0;
            })();
            break;
        case "clamp":
            result = execute(
                prepareInput(operation.value, answer, context, variableFor),
                (input) => {
                    const min = prepareInput(
                        operation.min,
                        answer,
                        context,
                        variableFor
                    );
                    const max = prepareInput(
                        operation.max,
                        answer,
                        context,
                        variableFor
                    );

                    input = isNumberFinite(min) ? Num.max(input, min) : input;

                    return isNumberFinite(max) ? Num.min(input, max) : input;
                }
            );
            break;
        case "cos":
            result = execute(
                prepareInput(operation.value, answer, context, variableFor),
                (input) =>
                    Math.cos(convertToRadians(input, operation.angleUnits))
            );
            break;
        case "cosh":
            result = execute(
                prepareInput(operation.value, answer, context, variableFor),
                (input) => Math.cosh(input)
            );
            break;
        case "count":
            result = (() => {
                const variable =
                    (isFilledString(operation.value) &&
                        variableFor(operation.value)) ||
                    undefined;

                return variable ? variable.refs.length : 0;
            })();
            break;
        case "date":
        case "datetime":
            result = (() => {
                const prepare = (v: number | undefined) => {
                    if (isNumberFinite(v)) {
                        const d = new Date(v);

                        if (operation.opcode === "datetime") {
                            d.setUTCSeconds(0);
                            d.setUTCMilliseconds(0);
                        } else {
                            d.setUTCHours(0);
                            d.setUTCMinutes(0);
                            d.setUTCSeconds(0);
                            d.setUTCMilliseconds(0);
                        }

                        return d.getTime();
                    }

                    return undefined;
                };
                const now = DateTime.UTC;
                const input = prepare(
                    prepareInput(operation.value, now, context, variableFor)
                );
                const value = prepare(
                    prepareInput(operation.cona, now, context, variableFor)
                );
                let match = false;

                switch (operation.compareMode) {
                    case "equal":
                        match = input === value;
                        break;
                    case "before":
                        match =
                            isNumberFinite(input) &&
                            isNumberFinite(value) &&
                            input < value;
                        break;
                    case "after":
                        match =
                            isNumberFinite(input) &&
                            isNumberFinite(value) &&
                            input > value;
                        break;
                    case "between":
                        const to = prepareInput(
                            operation.conb,
                            now,
                            context,
                            variableFor
                        );

                        match =
                            isNumberFinite(input) &&
                            isNumberFinite(value) &&
                            isNumberFinite(to) &&
                            input >= Num.min(value, to) &&
                            input <= Num.max(value, to);
                        break;
                    case "defined":
                        match = isNumberFinite(input);
                        break;
                }

                return prepareInput(
                    match ? operation.outa : operation.outb,
                    answer,
                    context,
                    variableFor
                );
            })();
            break;
        case "day-of-month":
        case "day-of-week":
        case "hour":
        case "millisecond":
        case "minute":
        case "month":
        case "second":
        case "year":
            result = (() => {
                const input =
                    isFilledString(operation.value) &&
                    variableFor(operation.value);

                if (
                    input &&
                    input.hasValue &&
                    input.slot instanceof Slots.Date
                ) {
                    const inputDate = input.slot.toDate(input.value);

                    switch (operation.opcode) {
                        case "year":
                            return inputDate.getUTCFullYear();
                        case "month":
                            return inputDate.getUTCMonth() + 1;
                        case "day-of-month":
                            return inputDate.getUTCDate();
                        case "day-of-week":
                            return inputDate.getUTCDay();
                        case "hour":
                            return inputDate.getUTCHours();
                        case "minute":
                            return inputDate.getUTCMinutes();
                        case "second":
                            return inputDate.getUTCSeconds();
                        case "millisecond":
                            return inputDate.getUTCMilliseconds();
                    }
                }

                return undefined;
            })();
            break;
        case "equation":
            result = (() => {
                const input = prepareInput(
                    operation.value,
                    answer,
                    context,
                    variableFor
                );
                const value = prepareInput(
                    operation.cona,
                    answer,
                    context,
                    variableFor
                );
                let match = false;

                switch (operation.compareMode) {
                    case "equal":
                        match = input === value;
                        break;
                    case "below":
                        match =
                            isNumberFinite(input) &&
                            isNumberFinite(value) &&
                            input < value;
                        break;
                    case "above":
                        match =
                            isNumberFinite(input) &&
                            isNumberFinite(value) &&
                            input > value;
                        break;
                    case "between":
                        const to = prepareInput(
                            operation.conb,
                            answer,
                            context,
                            variableFor
                        );

                        match =
                            isNumberFinite(input) &&
                            isNumberFinite(value) &&
                            isNumberFinite(to) &&
                            input >= Num.min(value, to) &&
                            input <= Num.max(value, to);
                        break;
                    case "defined":
                        match = isNumberFinite(input);
                        break;
                }

                return prepareInput(
                    match ? operation.outa : operation.outb,
                    answer,
                    context,
                    variableFor
                );
            })();
            break;
        case "evaluate":
            result = (() => {
                let match = false;
                const variable =
                    isFilledString(operation.value) &&
                    variableFor(operation.value);

                if (variable && variable.hasValue) {
                    const input = operation.ignoreCase
                        ? Str.lowercase(variable.string)
                        : variable.string;
                    const value = ((s) =>
                        operation.ignoreCase ? Str.lowercase(s) : s)(
                        parseVariables(
                            isFilledString(operation.cona) ? operation.cona : ""
                        )
                    );

                    switch (operation.compareMode) {
                        case "equal":
                            match = input === value;
                            break;
                        case "contains":
                            match =
                                (value && input.indexOf(value) !== -1) || false;
                            break;
                        case "starts":
                            match =
                                (value && input.indexOf(value) === 0) || false;
                            break;
                        case "ends":
                            match =
                                (value &&
                                    input.lastIndexOf(value) ===
                                        input.length - value.length) ||
                                false;
                            break;
                        case "defined":
                            match = input !== "";
                            break;
                    }
                }

                return prepareInput(
                    match ? operation.outa : operation.outb,
                    answer,
                    context,
                    variableFor
                );
            })();
            break;
        case "exp":
            result = execute(
                prepareInput(operation.exponent, answer, context, variableFor),
                (exponent) => Math.exp(exponent)
            );
            break;
        case "fact":
            result = execute(
                prepareInput(operation.value, answer, context, variableFor),
                factorial
            );
            break;
        case "floor":
            result = execute(
                prepareInput(operation.value, answer, context, variableFor),
                (input) => Num.floor(input)
            );
            break;
        case "gamma":
            result = execute(
                prepareInput(operation.value, answer, context, variableFor),
                gamma
            );
            break;
        case "lines":
            result = (() => {
                const variable =
                    isFilledString(operation.value) &&
                    variableFor(operation.value);

                return variable && variable.hasValue
                    ? (variable.string && variable.string.split("\n").length) ||
                          0
                    : 0;
            })();
            break;
        case "ln":
            result = execute(
                prepareInput(operation.value, answer, context, variableFor),
                (input) => Math.log(input)
            );
            break;
        case "log":
            result = execute(
                prepareInput(operation.value, answer, context, variableFor),
                (input) => Math.log10(input)
            );
            break;
        case "max":
            result = execute(
                prepareInput(operation.value, answer, context, variableFor),
                (input) => {
                    const max = prepareInput(
                        operation.max,
                        answer,
                        context,
                        variableFor
                    );

                    return isNumberFinite(max) ? Num.max(input, max) : input;
                }
            );
            break;
        case "min":
            result = execute(
                prepareInput(operation.value, answer, context, variableFor),
                (input) => {
                    const min = prepareInput(
                        operation.min,
                        answer,
                        context,
                        variableFor
                    );

                    return isNumberFinite(min) ? Num.min(input, min) : input;
                }
            );
            break;
        case "mod":
            result = execute(
                prepareInput(operation.value, answer, context, variableFor),
                (input) => {
                    const divisor = prepareInput(
                        operation.divisor,
                        answer,
                        context,
                        variableFor
                    );

                    return isNumberFinite(divisor) && divisor !== 0
                        ? input % divisor
                        : undefined;
                }
            );
            break;
        case "number":
            result = prepareInput(
                operation.value,
                answer,
                context,
                variableFor
            );
            break;
        case "occurrences":
            result = (() => {
                const variable =
                    isFilledString(operation.value) &&
                    variableFor(operation.value);

                if (variable && variable.hasValue) {
                    if (operation.compareMode === "regex") {
                        try {
                            const regex =
                                (isFilledString(operation.cona) &&
                                    operation.cona) ||
                                "";
                            const literalSignLeft = regex.indexOf("/");
                            const literalSignRight = regex.lastIndexOf("/");

                            return (
                                (literalSignLeft === 0 &&
                                    literalSignRight > literalSignLeft &&
                                    ((v: string) => {
                                        try {
                                            return (
                                                v.match(
                                                    new RegExp(
                                                        regex.substring(
                                                            1,
                                                            literalSignRight
                                                        ),
                                                        regex.substr(
                                                            literalSignRight + 1
                                                        )
                                                    )
                                                ) || []
                                            ).length;
                                        } catch {
                                            return 0;
                                        }
                                    })(variable.string)) ||
                                0
                            );
                        } catch {
                            return 0;
                        }
                    } else {
                        const searchFor =
                            (isFilledString(operation.cona) &&
                                (operation.ignoreCase
                                    ? Str.lowercase(operation.cona)
                                    : operation.cona)) ||
                            "";
                        const input = operation.ignoreCase
                            ? Str.lowercase(variable.string)
                            : variable.string;

                        return searchFor && input
                            ? input.split(searchFor).length - 1
                            : 0;
                    }
                }

                return 0;
            })();
            break;
        case "percentage":
            result = execute(
                prepareInput(operation.value, answer, context, variableFor),
                (input) => {
                    const percentage = prepareInput(
                        operation.percentage,
                        answer,
                        context,
                        variableFor
                    );

                    return isNumberFinite(percentage)
                        ? (input / 100) * percentage
                        : undefined;
                }
            );
            break;
        case "pow":
            result = execute(
                prepareInput(operation.value, answer, context, variableFor),
                (input) => {
                    const exponent = prepareInput(
                        operation.exponent,
                        answer,
                        context,
                        variableFor
                    );

                    return isNumberFinite(exponent)
                        ? Math.pow(input, exponent)
                        : undefined;
                }
            );
            break;
        case "round":
            result = execute(
                prepareInput(operation.value, answer, context, variableFor),
                (input) => Num.round(input)
            );
            break;
        case "score":
            result = (() => {
                const variable =
                    (isFilledString(operation.value) &&
                        variableFor(operation.value)) ||
                    undefined;

                if (variable && operation.scores) {
                    let score = 0;

                    each(variable.refs, (ref) => {
                        score += operation.scores![ref] || 0;
                    });

                    return score;
                }

                return undefined;
            })();
            break;
        case "sgn":
            result = execute(
                prepareInput(operation.value, answer, context, variableFor),
                (input) => Math.sign(input)
            );
            break;
        case "sin":
            result = execute(
                prepareInput(operation.value, answer, context, variableFor),
                (input) =>
                    Math.sin(convertToRadians(input, operation.angleUnits))
            );
            break;
        case "sinh":
            result = execute(
                prepareInput(operation.value, answer, context, variableFor),
                (input) => Math.sinh(input)
            );
            break;
        case "sqrt":
            result = execute(
                prepareInput(operation.value, answer, context, variableFor),
                (input) => Math.sqrt(input)
            );
            break;
        case "square":
            result = execute(
                prepareInput(operation.value, answer, context, variableFor),
                (input) => input * input
            );
            break;
        case "sum":
            result = (() => {
                const variable =
                    (isFilledString(operation.value) &&
                        variableFor(operation.value)) ||
                    undefined;

                return variable ? castToFloat(variable.value) : undefined;
            })();
            break;
        case "tan":
            result = execute(
                prepareInput(operation.value, answer, context, variableFor),
                (input) =>
                    Math.tan(convertToRadians(input, operation.angleUnits))
            );
            break;
        case "tanh":
            result = execute(
                prepareInput(operation.value, answer, context, variableFor),
                (input) => Math.tanh(input)
            );
            break;
        case "trunc":
            result = execute(
                prepareInput(operation.value, answer, context, variableFor),
                (input) => Math.trunc(input)
            );
            break;
        case "words":
            result = (() => {
                const variable =
                    isFilledString(operation.value) &&
                    variableFor(operation.value);

                return variable && variable.hasValue
                    ? variable.string.split(" ").filter((w) => w !== "").length
                    : 0;
            })();
            break;
        default:
            return undefined;
    }

    switch (operator) {
        case "+":
            return isNumberFinite(answer) || isNumberFinite(result)
                ? (answer || 0) + (result || 0)
                : undefined;
        case "-":
            return isNumberFinite(answer) || isNumberFinite(result)
                ? (answer || 0) - (result || 0)
                : undefined;
        case "*":
            return isNumberFinite(answer) && isNumberFinite(result)
                ? answer * result
                : undefined;
        case "/":
            return isNumberFinite(answer) &&
                isNumberFinite(result) &&
                result !== 0
                ? answer / result
                : undefined;
        case "=":
            return result;
    }
}

export function calculator(
    context: Context,
    operations: IOperation[],
    startSlot: Value<number, Slots.Number | Slots.Numeric> | undefined,
    resultSlot: Value<number, Slots.Number | Slots.Numeric> | undefined,
    variableFor: (id: string) => IVariable | undefined,
    parseVariables: (id: string) => string,
    getSlot?: (id: string) => Value | undefined
): number | undefined {
    let result: number | undefined =
        startSlot && startSlot.hasValue
            ? startSlot.slot.toValue(startSlot.value)
            : undefined;

    if (!startSlot || isNumberFinite(result)) {
        const count = arraySize(operations);

        if (count > 0) {
            if (!startSlot) {
                result = 0;
            }

            for (let i = 0; i < count; i++) {
                const operation = operations[i];
                const operator =
                    i === 0 && !startSlot ? "=" : operation.operator;

                result = process(
                    context,
                    operator,
                    operation,
                    result,
                    variableFor,
                    parseVariables,
                    getSlot
                );

                if (
                    !isNumberFinite(result) &&
                    (operator === "*" || operator === "/")
                ) {
                    break;
                }
            }
        }
    }

    if (resultSlot) {
        resultSlot.set(result);
    }

    return result;
}
