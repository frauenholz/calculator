export type TAngles = "degrees" | "radians" | "gradians";

export function convertToRadians(angle: number, angleUnits?: TAngles): number {
    switch (angleUnits) {
        case "radians":
            return angle;
        case "gradians":
            return angle * (Math.PI / 200);
        default:
            return angle * (Math.PI / 180);
    }
}

export function convertAngle(radians: number, angleUnits?: TAngles): number {
    switch (angleUnits) {
        case "radians":
            return radians;
        case "gradians":
            return radians * (200 / Math.PI);
        default:
            return radians * (180 / Math.PI);
    }
}
