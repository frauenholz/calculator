/** Package information retrieved from `package.json` using webpack. */
declare const PACKAGE_NAME: string;

/** Dependencies */
import {
    ConditionBlock,
    Num,
    Slots,
    condition,
    isNumberFinite,
    isString,
    tripetto,
} from "tripetto-runner-foundation";
import { TModes } from "./modes";

@tripetto({
    type: "condition",
    identifier: PACKAGE_NAME,
})
export class CalculatorCondition extends ConditionBlock<{
    readonly mode: TModes;
    readonly value?: number | string;
    readonly to?: number | string;
}> {
    private getValue(slot: Slots.Slot, value: number | string | undefined) {
        if (isString(value) && slot instanceof Slots.Numeric) {
            const variable = this.variableFor(value);

            return variable && variable.hasValue
                ? slot.toValue(variable.value)
                : undefined;
        }

        return isNumberFinite(value) ? value : undefined;
    }

    @condition
    verify(): boolean {
        const calculatorSlot = this.valueOf<number>();

        if (calculatorSlot) {
            const value = this.getValue(calculatorSlot.slot, this.props.value);

            switch (this.props.mode) {
                case "equal":
                    return (
                        (calculatorSlot.hasValue
                            ? calculatorSlot.value
                            : undefined) === value
                    );
                case "not-equal":
                    return (
                        (calculatorSlot.hasValue
                            ? calculatorSlot.value
                            : undefined) !== value
                    );
                case "below":
                    return (
                        isNumberFinite(value) &&
                        calculatorSlot.hasValue &&
                        calculatorSlot.value < value
                    );
                case "above":
                    return (
                        isNumberFinite(value) &&
                        calculatorSlot.hasValue &&
                        calculatorSlot.value > value
                    );
                case "between":
                case "not-between":
                    const to = this.getValue(
                        calculatorSlot.slot,
                        this.props.to
                    );

                    return (
                        isNumberFinite(value) &&
                        isNumberFinite(to) &&
                        (calculatorSlot.hasValue &&
                            calculatorSlot.value >= Num.min(value, to) &&
                            calculatorSlot.value <= Num.max(value, to)) ===
                            (this.props.mode === "between")
                    );
                case "defined":
                    return calculatorSlot.hasValue;
                case "undefined":
                    return !calculatorSlot.hasValue;
            }
        }

        return false;
    }
}
