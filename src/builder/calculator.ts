/** Package information retrieved from `package.json` using webpack. */
declare const PACKAGE_NAME: string;
declare const PACKAGE_VERSION: string;

/** Dependencies */
import {
    Collection,
    ConditionsOrchestrator,
    Forms,
    Node,
    NodeBlock,
    Num,
    Slots,
    affects,
    assigned,
    castToNumber,
    conditions,
    definition,
    each,
    editor,
    getHelpTopic,
    isNumber,
    isString,
    npgettext,
    pgettext,
    renamed,
    scheduleTimeout,
    set,
    slots,
    tripetto,
} from "tripetto";
import { ICalculator } from "./interface";
import { Operation } from "./operation";
import { calculator } from "./collection";
import { TModes } from "../runner/modes";
import { CalculatorCondition } from "./condition";

/** Assets */
import ICON from "../../assets/icon.svg";

@tripetto({
    type: "node",
    identifier: PACKAGE_NAME,
    version: PACKAGE_VERSION,
    icon: ICON,
    get label() {
        return pgettext("block:calculator", "Calculator");
    },
    kind: "headless",
})
export class Calculator extends NodeBlock implements ICalculator {
    readonly allowMarkdown = false;
    readonly startBlank = true;
    outcome!: Slots.Numeric;
    preselect?: (slot: Slots.Numeric) => void;

    @definition
    @affects("#name")
    readonly operations: Collection.Provider<Operation, ICalculator> =
        Collection.of<Operation, ICalculator>(Operation, this);

    get block() {
        return this;
    }

    get label() {
        return npgettext(
            "block:calculator",
            "%2 (%1 operation)",
            "%2 (%1 operations)",
            this.operations.count,
            this.type.label
        );
    }

    @assigned
    defineName(): void {
        if (!this.node.name && !this.operations.count) {
            let count = 0;
            let index = 1;

            this.map?.forEach((node: Node) => {
                if (node.blockTypeIdentifier === PACKAGE_NAME) {
                    count++;

                    if (
                        node.name &&
                        node.name.indexOf(Calculator.label + " ") === 0
                    ) {
                        const c = castToNumber(
                            node.name.substr(Calculator.label.length + 1)
                        );

                        if (c > index) {
                            index = c + 1;
                        }
                    }
                }
            }, "nodes");

            this.node.name = Calculator.label + ` ${Num.max(index, count)}`;
        }

        this.outcome.hook(
            "OnSlotProperty",
            "synchronous",
            (hook: Slots.ISlotPropertyEvent<Slots.Numeric>) => {
                switch (hook.property) {
                    case "precision":
                    case "digits":
                    case "decimal":
                    case "separator":
                    case "minimum":
                    case "maximum":
                    case "prefix":
                    case "prefixPlural":
                    case "suffix":
                    case "suffixPlural":
                        this.slots.each((slot) => {
                            if (
                                slot instanceof Slots.Numeric &&
                                slot.kind === "dynamic"
                            ) {
                                set(
                                    slot,
                                    hook.property,
                                    this.outcome[hook.property]
                                );
                            }
                        });
                        break;
                }
            },
            this
        );
    }

    @slots
    @renamed
    defineSlot(): void {
        this.outcome = this.slots.static({
            type: Slots.Numeric,
            reference: "calculator",
            label:
                this.node.name || pgettext("block:calculator", "Calculation"),
            exchange: [
                "alias",
                "exportable",
                "precision",
                "digits",
                "decimal",
                "separator",
                "minimum",
                "maximum",
                "prefix",
                "prefixPlural",
                "suffix",
                "suffixPlural",
            ],
        });
    }

    @editor
    defineEditor(): void {
        const minimum = new Forms.Numeric(
            Forms.Numeric.bind(this.outcome, "minimum", undefined)
        )
            .precision(this.outcome.precision || 0)
            .digits(this.outcome.digits || 0)
            .decimalSign(this.outcome.decimal || "")
            .thousands(
                this.outcome.separator ? true : false,
                this.outcome.separator || ""
            )
            .prefix(this.outcome.prefix || "")
            .prefixPlural(this.outcome.prefixPlural || undefined)
            .suffix(this.outcome.suffix || "")
            .suffixPlural(this.outcome.suffixPlural || undefined)
            .label(pgettext("block:calculator", "Minimum"));
        const maximum = new Forms.Numeric(
            Forms.Numeric.bind(this.outcome, "maximum", undefined)
        )
            .precision(this.outcome.precision || 0)
            .digits(this.outcome.digits || 0)
            .decimalSign(this.outcome.decimal || "")
            .thousands(
                this.outcome.separator ? true : false,
                this.outcome.separator || ""
            )
            .prefix(this.outcome.prefix || "")
            .prefixPlural(this.outcome.prefixPlural || undefined)
            .suffix(this.outcome.suffix || "")
            .suffixPlural(this.outcome.suffixPlural || undefined)
            .label(pgettext("block:calculator", "Maximum"));

        const preselectNotification = this.editor
            .form({
                controls: [
                    new Forms.Notification(
                        pgettext(
                            "block:calculator",
                            "Some formatting options were automatically set for you, based on the block you've just selected as initial value."
                        ),
                        "success"
                    ),
                ],
            })
            .visible(false);

        const helpTopic = getHelpTopic("block:calculator");

        this.editor.form({
            title: pgettext("block:calculator", "Explanation"),
            controls: [
                new Forms.Static(
                    pgettext(
                        "block:calculator",
                        "Perform a calculation and use the result in your form%1.",
                        helpTopic
                            ? ` ([${pgettext(
                                  "block:calculator",
                                  "learn more"
                              )}](${helpTopic}))`
                            : ""
                    )
                ).markdown(),
            ],
        });

        this.editor.option({
            name: pgettext("block:calculator", "Operations"),
            collection: calculator(this),
            activated: true,
            locked: true,
        });

        this.editor.name(
            false,
            false,
            pgettext("block:calculator", "Description"),
            false
        );

        this.editor.groups.settings();
        const format = new Forms.Dropdown(
            [
                {
                    optGroup: pgettext("block:calculator", "Decimals"),
                },
                { label: "#", value: 0 },
                { label: "#.#", value: 1 },
                { label: "#.##", value: 2 },
                { label: "#.###", value: 3 },
                { label: "#.####", value: 4 },
                { label: "#.#####", value: 5 },
                { label: "#.######", value: 6 },
                { label: "#.#######", value: 7 },
                { label: "#.########", value: 8 },
                {
                    optGroup: pgettext("block:calculator", "Digits"),
                },
                { label: "##", value: -2 },
                { label: "###", value: -3 },
                { label: "####", value: -4 },
                { label: "#####", value: -5 },
                { label: "######", value: -6 },
                { label: "#######", value: -7 },
                { label: "########", value: -8 },
                { label: "#########", value: -9 },
                { label: "##########", value: -10 },
                { label: "###########", value: -11 },
                { label: "############", value: -12 },
                { label: "#############", value: -13 },
                { label: "##############", value: -14 },
                { label: "###############", value: -15 },
                { label: "################", value: -16 },
            ],
            (this.outcome.digits
                ? -this.outcome.digits
                : this.outcome.precision) || 0
        ).on(() => {
            this.outcome.precision =
                format.isFeatureEnabled &&
                isNumber(format.value) &&
                format.value >= 0
                    ? format.value
                    : undefined;
            this.outcome.digits =
                format.isFeatureEnabled &&
                isNumber(format.value) &&
                format.value < 0
                    ? -format.value
                    : undefined;

            minimum.precision(this.outcome.precision || 0);
            maximum.precision(this.outcome.precision || 0);
            minimum.digits(this.outcome.digits || 0);
            maximum.digits(this.outcome.digits || 0);
            decimals.disabled(!this.outcome.precision);
            signsForm.disabled((this.outcome.digits || 0) > 0);
        });
        const formatForm = this.editor.option({
            name: pgettext("block:calculator", "Format"),
            form: {
                title: pgettext("block:calculator", "Format"),
                controls: [format],
            },
            activated:
                isNumber(this.outcome.precision) ||
                isNumber(this.outcome.digits),
        });

        const decimals = new Forms.Dropdown(
            [
                { label: "#.#", value: "." },
                { label: "#,#", value: "," },
            ],
            Forms.Dropdown.bind(this.outcome, "decimal", undefined)
        )
            .label(pgettext("block:calculator", "Decimal sign"))
            .disabled(!this.outcome.precision)
            .on((decimal) => {
                minimum.decimalSign(
                    (decimal.isFeatureEnabled && decimal.value) || ""
                );
                maximum.decimalSign(
                    (decimal.isFeatureEnabled && decimal.value) || ""
                );
            });
        const thousands = new Forms.Dropdown(
            [
                {
                    label: pgettext("block:calculator", "None"),
                    value: undefined,
                },
                { label: "#,###", value: "," },
                { label: "#.###", value: "." },
            ],
            Forms.Dropdown.bind(this.outcome, "separator", undefined)
        )
            .label(pgettext("block:calculator", "Thousands separator"))
            .on((separator) => {
                minimum.thousands(
                    separator.isFeatureEnabled && separator.value
                        ? true
                        : false,
                    (separator.isFeatureEnabled && separator.value) || ""
                );
                maximum.thousands(
                    separator.isFeatureEnabled && separator.value
                        ? true
                        : false,
                    (separator.isFeatureEnabled && separator.value) || ""
                );
            });

        this.editor.option({
            name: pgettext("block:calculator", "Limits"),
            form: {
                title: pgettext("block:calculator", "Limits"),
                controls: [minimum, maximum],
            },
            activated:
                isNumber(this.outcome.minimum) ||
                isNumber(this.outcome.maximum),
        });

        const prefix = new Forms.Text(
            "singleline",
            Forms.Text.bind(this.outcome, "prefix", undefined)
        )
            .sanitize(false)
            .on((p: Forms.Text) => {
                minimum.prefix((p.isFeatureEnabled && p.value) || "");
                maximum.prefix((p.isFeatureEnabled && p.value) || "");
            });
        const prefixPlural = new Forms.Text(
            "singleline",
            Forms.Text.bind(this.outcome, "prefixPlural", undefined)
        )
            .indent(32)
            .sanitize(false)
            .on((p: Forms.Text) => {
                minimum.prefixPlural(
                    (p.isFeatureEnabled && p.isObservable && p.value) ||
                        undefined
                );
                maximum.prefixPlural(
                    (p.isFeatureEnabled && p.isObservable && p.value) ||
                        undefined
                );
            })
            .placeholder(
                pgettext("block:calculator", "Prefix when value is plural")
            )
            .visible(isString(this.outcome.prefixPlural));
        const prefixPluralEnabled = new Forms.Checkbox(
            pgettext(
                "block:calculator",
                "Specify different prefix for plural values"
            ),
            isString(this.outcome.prefixPlural)
        ).on((c) => {
            prefix.placeholder(
                (c.isChecked &&
                    pgettext(
                        "block:calculator",
                        "Prefix when value is singular"
                    )) ||
                    ""
            );

            prefixPlural.visible(c.isChecked);
        });

        const prefixForm = this.editor.option({
            name: pgettext("block:calculator", "Prefix"),
            form: {
                title: pgettext("block:calculator", "Prefix"),
                controls: [prefix, prefixPluralEnabled, prefixPlural],
            },
            activated: isString(this.outcome.prefix),
        });

        const suffix = new Forms.Text(
            "singleline",
            Forms.Text.bind(this.outcome, "suffix", undefined)
        )
            .sanitize(false)
            .on((s: Forms.Text) => {
                minimum.suffix((s.isFeatureEnabled && s.value) || "");
                maximum.suffix((s.isFeatureEnabled && s.value) || "");
            });
        const suffixPlural = new Forms.Text(
            "singleline",
            Forms.Text.bind(this.outcome, "suffixPlural", undefined)
        )
            .indent(32)
            .sanitize(false)
            .on((s: Forms.Text) => {
                minimum.suffixPlural(
                    (s.isFeatureEnabled && s.isObservable && s.value) ||
                        undefined
                );
                maximum.suffixPlural(
                    (s.isFeatureEnabled && s.isObservable && s.value) ||
                        undefined
                );
            })
            .placeholder(
                pgettext("block:calculator", "Suffix when value is plural")
            )
            .visible(isString(this.outcome.suffixPlural));
        const suffixPluralEnabled = new Forms.Checkbox(
            pgettext(
                "block:calculator",
                "Specify different suffix for plural values"
            ),
            isString(this.outcome.suffixPlural)
        ).on((c) => {
            suffix.placeholder(
                (c.isChecked &&
                    pgettext(
                        "block:calculator",
                        "Suffix when value is singular"
                    )) ||
                    ""
            );

            suffixPlural.visible(c.isChecked);
        });

        const suffixForm = this.editor.option({
            name: pgettext("block:calculator", "Suffix"),
            form: {
                title: pgettext("block:calculator", "Suffix"),
                controls: [suffix, suffixPluralEnabled, suffixPlural],
            },
            activated: isString(this.outcome.suffix),
        });

        const signsForm = this.editor.option({
            name: pgettext("block:calculator", "Signs"),
            form: {
                title: pgettext("block:calculator", "Signs"),
                controls: [
                    decimals,
                    thousands,
                    new Forms.Static(
                        pgettext(
                            "block:calculator",
                            "**Note:** These signs are used to format the number in de dataset. When the number is displayed in a runner, the appropriate user locale might be applied making it seem like changing these settings has no effect."
                        )
                    ).markdown(),
                ],
            },
            activated:
                isString(this.outcome.separator) ||
                isString(this.outcome.decimal),
            disabled: (this.outcome.digits || 0) > 0,
        });

        this.editor.groups.options();
        this.editor.visibility();
        this.editor.alias(this.outcome);
        this.editor.exportable(this.outcome);

        this.preselect = (slot) => {
            let showNotification = false;

            if ((slot.precision || slot.digits) && !formatForm.isActivated) {
                format.value = slot.digits ? -slot.digits : slot.precision;
                showNotification = true;

                formatForm.activate();
            }

            if ((slot.decimal || slot.separator) && !signsForm.isActivated) {
                decimals.value = slot.decimal;
                thousands.value = slot.decimal;
                showNotification = true;

                signsForm.activate();
            }

            if ((slot.prefix || slot.prefixPlural) && !prefixForm.isActivated) {
                prefix.value = slot.prefix || "";
                showNotification = true;

                if (slot.prefixPlural) {
                    prefixPlural.value = slot.prefixPlural;
                    prefixPluralEnabled.check();
                }

                prefixForm.activate();
            }

            if ((slot.suffix || slot.suffixPlural) && !suffixForm.isActivated) {
                suffix.value = slot.suffix || "";
                showNotification = true;

                if (slot.suffixPlural) {
                    suffixPlural.value = slot.suffixPlural;
                    suffixPluralEnabled.check();
                }

                suffixForm.activate();
            }

            if (showNotification) {
                preselectNotification.visible(true);

                scheduleTimeout(() => {
                    preselectNotification.visible(false);
                }, 10000);
            }

            this.preselect = undefined;
        };
    }

    @conditions
    defineCondition(): void {
        const fnTemplates = (group: ConditionsOrchestrator, slot: Slots.Slot) =>
            each(
                [
                    {
                        mode: "equal",
                        label: pgettext(
                            "block:calculator",
                            "Calculation is equal to"
                        ),
                    },
                    {
                        mode: "not-equal",
                        label: pgettext(
                            "block:calculator",
                            "Calculation is not equal to"
                        ),
                    },
                    {
                        mode: "below",
                        label: pgettext(
                            "block:calculator",
                            "Calculation is lower than"
                        ),
                    },
                    {
                        mode: "above",
                        label: pgettext(
                            "block:calculator",
                            "Calculation is higher than"
                        ),
                    },
                    {
                        mode: "between",
                        label: pgettext(
                            "block:calculator",
                            "Calculation is between"
                        ),
                    },
                    {
                        mode: "not-between",
                        label: pgettext(
                            "block:calculator",
                            "Calculation is not between"
                        ),
                    },
                    {
                        mode: "defined",
                        label: pgettext(
                            "block:calculator",
                            "Calculation is valid"
                        ),
                    },
                    {
                        mode: "undefined",
                        label: pgettext(
                            "block:calculator",
                            "Calculation is not valid"
                        ),
                    },
                ],
                (condition: { mode: TModes; label: string }) => {
                    group.template({
                        condition: CalculatorCondition,
                        label: condition.label,
                        autoOpen:
                            condition.mode !== "defined" &&
                            condition.mode !== "undefined",
                        props: {
                            slot,
                            mode: condition.mode,
                            value: 0,
                            to:
                                condition.mode === "between" ||
                                condition.mode === "not-between"
                                    ? 0
                                    : undefined,
                        },
                    });
                }
            );

        if (this.slots.count > 1) {
            this.slots.each((slot) => {
                if (slot.label) {
                    fnTemplates(this.conditions.group(slot.label), slot);
                }
            });
        } else {
            fnTemplates(this.conditions, this.outcome);
        }
    }
}
