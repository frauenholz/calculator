/** Dependencies */
import { Collection, NodeBlock, Slots } from "tripetto";
import { Operation } from "./operation";

export interface ICalculator {
    /** Reference to the block that holds the calculation. */
    readonly block: NodeBlock;

    /** Contains the operations of the calculation. */
    readonly operations: Collection.Provider<Operation, ICalculator>;

    /** Specifies if the calculator always starts blank. */
    readonly startBlank: boolean;

    /** Specifies the first answer when a calculator begins with an initial value. */
    readonly firstANS?: string;

    /** Reference to the slot that will receive the outcome. */
    readonly outcome?: Slots.Numeric;

    /** Optional reference to a preselect function that is used to configure the outcome slot. */
    readonly preselect?: (slot: Slots.Numeric) => void;
}
