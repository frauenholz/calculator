/** Dependencies */
import {
    Collection,
    Components,
    EditorOrchestrator,
    L10n,
    Slots,
    affects,
    created,
    definition,
    deleted,
    editor,
    icon,
    isNumberFinite,
    isString,
    lookupVariable,
    menu,
    name,
    pgettext,
    setAny,
    title,
    width,
} from "tripetto";
import { ICalculator } from "./interface";
import { TOperators } from "../runner/operators";
import { TOpcodes } from "../runner/opcodes";
import { isConstant } from "../runner/constants";
import { TModes } from "../runner/modes";
import { TAngles } from "../runner/angles";
import { TAge } from "../runner/age";
import { IScores } from "../runner/scores";
import { operatorChangeMenu } from "./menu";

/** Operations */
import { absOperation } from "./operations/abs";
import { acoshOperation } from "./operations/acosh";
import { acosOperation } from "./operations/acos";
import { ageOperation } from "./operations/age";
import { asinhOperation } from "./operations/asinh";
import { asinOperation } from "./operations/asin";
import { atanhOperation } from "./operations/atanh";
import { atanOperation } from "./operations/atan";
import { booleanOperation } from "./operations/boolean";
import { calculatorOperation } from "./operations/calculator";
import { castOperation } from "./operations/cast";
import { ceilOperation } from "./operations/ceil";
import { charsOperation } from "./operations/chars";
import { clampOperation } from "./operations/clamp";
import { coshOperation } from "./operations/cosh";
import { cosOperation } from "./operations/cos";
import { countOperation } from "./operations/count";
import { dateOperation } from "./operations/date";
import { dayOfMonthOperation } from "./operations/day-of-month";
import { dayOfWeekOperation } from "./operations/day-of-week";
import { equationOperation } from "./operations/equation";
import { evaluateOperation } from "./operations/evaluate";
import { expOperation } from "./operations/exp";
import { factorialOperation } from "./operations/factorial";
import { floorOperation } from "./operations/floor";
import { gammaOperation } from "./operations/gamma";
import { hourOperation } from "./operations/hour";
import { linesOperation } from "./operations/lines";
import { lnOperation } from "./operations/ln";
import { logOperation } from "./operations/log";
import { maxOperation } from "./operations/max";
import { millisecondOperation } from "./operations/millisecond";
import { minOperation } from "./operations/min";
import { minuteOperation } from "./operations/minute";
import { modOperation } from "./operations/mod";
import { monthOperation } from "./operations/month";
import { numberOperation } from "./operations/number";
import { occurrencesOperation } from "./operations/occurrences";
import { percentageOperation } from "./operations/percentage";
import { powOperation } from "./operations/pow";
import { roundOperation } from "./operations/round";
import { scoreOperation } from "./operations/score";
import { secondOperation } from "./operations/second";
import { sgnOperation } from "./operations/sgn";
import { sinhOperation } from "./operations/sinh";
import { sinOperation } from "./operations/sin";
import { sqrtOperation } from "./operations/sqrt";
import { squareOperation } from "./operations/square";
import { sumOperation } from "./operations/sum";
import { tanhOperation } from "./operations/tanh";
import { tanOperation } from "./operations/tan";
import { truncOperation } from "./operations/trunc";
import { wordsOperation } from "./operations/words";
import { yearOperation } from "./operations/year";

/** Icons */
import ICON from "../../assets/icon.svg";
import ICON_INITIAL from "../../assets/initial.svg";
import ICON_EQUAL from "../../assets/equal.svg";
import ICON_ADD from "../../assets/add.svg";
import ICON_SUBTRACT from "../../assets/subtract.svg";
import ICON_MULTIPLY from "../../assets/multiply.svg";
import ICON_DIVIDE from "../../assets/divide.svg";

export interface IOperation {
    readonly name: (operation: Operation) => string | undefined;
    readonly title: (operation: Operation) => string | undefined;
    readonly editor: (
        operation: Operation,
        editor: EditorOrchestrator<Operation>
    ) => void;
}

export class Operation
    extends Collection.Item<ICalculator>
    implements ICalculator
{
    readonly startBlank = false;

    get calculator(): ICalculator {
        return this.ref;
    }

    get block() {
        return this.calculator.block;
    }

    get outcome(): Slots.Numeric | undefined {
        return (
            (this.opcode === "calc" &&
                this.calculator.block.slots.select(this.id, "dynamic")) ||
            undefined
        );
    }

    @icon
    get icon() {
        if (
            this.calculator.startBlank &&
            this.isFirst &&
            !this.isSubCalculation
        ) {
            return ICON_INITIAL;
        }

        switch (this.operator) {
            case "+":
                return ICON_ADD;
            case "-":
                return ICON_SUBTRACT;
            case "*":
                return ICON_MULTIPLY;
            case "/":
                return ICON_DIVIDE;
            default:
                return ICON_EQUAL;
        }
    }

    @name
    get name() {
        const s = Operation.get(this.opcode)?.name(this) || "";

        if (!this.isFirst && this.operator === "/" && s === "0") {
            return s + " *(division by zero error)*";
        }

        return s;
    }

    @title
    get title() {
        return Operation.get(this.opcode)?.title(this) || "";
    }

    @width
    get width() {
        switch (this.opcode) {
            case "calc":
                return 520;
            case "date":
            case "datetime":
            case "age":
                return 475;
        }

        return 400;
    }

    @menu
    get menu() {
        return [
            new Components.MenuSubmenuWithImage(
                ICON,
                pgettext("block:calculator", "Operator"),
                operatorChangeMenu(this),
                this.calculator.startBlank && this.isFirst
            ),
        ];
    }

    @definition
    @affects("#icon")
    @affects("#name")
    operator: TOperators = "=";

    @definition
    @affects("#name")
    opcode: TOpcodes = "number";

    @definition
    @affects("#name")
    readonly operations: Collection.Provider<Operation, ICalculator> =
        Collection.of<Operation, ICalculator>(Operation, this);

    @definition
    @affects("#name")
    description?: string;

    @definition
    reference?: string | number;

    @definition
    @affects("#name")
    value?: string | number;

    @definition
    @affects("#name")
    exponent?: string | number;

    @definition
    @affects("#name")
    cona?: string | number;

    @definition
    @affects("#name")
    conb?: string | number;

    @definition
    @affects("#name")
    outa?: string | number;

    @definition
    @affects("#name")
    outb?: string | number;

    @definition
    @affects("#name")
    compareMode?: TModes;

    @definition
    @affects("#name")
    angleUnits?: TAngles;

    @definition
    @affects("#name")
    ageIn?: TAge;

    @definition
    ageAbs?: boolean;

    @definition
    ignoreCase?: boolean;

    @definition
    scores?: IScores;

    @definition
    @affects("#name")
    min?: string | number;

    @definition
    @affects("#name")
    max?: string | number;

    @definition
    @affects("#name")
    divisor?: string | number;

    @definition
    @affects("#name")
    percentage?: string | number;

    get isSubCalculation(): boolean {
        return (
            this.calculator.operations.itemAtIndex(this.index)?.id !== this.id
        );
    }

    get allowANS(): boolean {
        return (
            this.isSubCalculation ||
            !this.calculator.startBlank ||
            !this.isFirst
        );
    }

    static get(opcode: TOpcodes): IOperation {
        switch (opcode) {
            case "abs":
                return absOperation;
            case "acos":
                return acosOperation;
            case "acosh":
                return acoshOperation;
            case "age":
                return ageOperation;
            case "asin":
                return asinOperation;
            case "asinh":
                return asinhOperation;
            case "atan":
                return atanOperation;
            case "atanh":
                return atanhOperation;
            case "boolean":
                return booleanOperation;
            case "calc":
                return calculatorOperation;
            case "cast":
                return castOperation;
            case "ceil":
                return ceilOperation;
            case "chars":
                return charsOperation;
            case "clamp":
                return clampOperation;
            case "cos":
                return cosOperation;
            case "cosh":
                return coshOperation;
            case "count":
                return countOperation;
            case "date":
            case "datetime":
                return dateOperation;
            case "day-of-month":
                return dayOfMonthOperation;
            case "day-of-week":
                return dayOfWeekOperation;
            case "equation":
                return equationOperation;
            case "evaluate":
                return evaluateOperation;
            case "exp":
                return expOperation;
            case "fact":
                return factorialOperation;
            case "floor":
                return floorOperation;
            case "gamma":
                return gammaOperation;
            case "hour":
                return hourOperation;
            case "lines":
                return linesOperation;
            case "ln":
                return lnOperation;
            case "log":
                return logOperation;
            case "max":
                return maxOperation;
            case "millisecond":
                return millisecondOperation;
            case "min":
                return minOperation;
            case "minute":
                return minuteOperation;
            case "mod":
                return modOperation;
            case "month":
                return monthOperation;
            case "number":
                return numberOperation;
            case "occurrences":
                return occurrencesOperation;
            case "percentage":
                return percentageOperation;
            case "pow":
                return powOperation;
            case "round":
                return roundOperation;
            case "score":
                return scoreOperation;
            case "second":
                return secondOperation;
            case "sgn":
                return sgnOperation;
            case "sin":
                return sinOperation;
            case "sinh":
                return sinhOperation;
            case "sqrt":
                return sqrtOperation;
            case "square":
                return squareOperation;
            case "sum":
                return sumOperation;
            case "tan":
                return tanOperation;
            case "tanh":
                return tanhOperation;
            case "trunc":
                return truncOperation;
            case "year":
                return yearOperation;
            case "words":
                return wordsOperation;
        }
    }

    set(props: {
        readonly operator: TOperators;
        readonly opcode: TOpcodes;
        readonly value?: string | number;
        readonly cona?: string | number;
        readonly conb?: string | number;
        readonly outa?: string | number;
        readonly outb?: string | number;
        readonly min?: string | number;
        readonly max?: string | number;
        readonly exponent?: string | number;
        readonly divisor?: string | number;
        readonly percentage?: string | number;
        readonly ageAbs?: boolean;
        readonly open?: boolean;
    }): this {
        this.operator = props.operator;
        this.opcode = props.opcode;
        this.value = props.value;
        this.cona = props.cona;
        this.conb = props.conb;
        this.outa = props.outa;
        this.outb = props.outb;
        this.min = props.min;
        this.max = props.max;
        this.exponent = props.exponent;
        this.divisor = props.divisor;
        this.percentage = props.percentage;
        this.ageAbs = props.ageAbs;

        if (
            this.calculator.preselect &&
            this.isFirst &&
            this.opcode === "number" &&
            isString(this.value)
        ) {
            const slot = lookupVariable(
                this.calculator.block,
                this.value
            )?.slot;

            if (slot instanceof Slots.Numeric) {
                this.calculator.preselect(slot);
            }
        }

        if (props.open) {
            this.open();
        }

        return this;
    }

    format(
        property: keyof this,
        prefix?: string,
        suffix?: string,
        hasNumericSuffix?: boolean,
        boldNumbers: boolean = false,
        defaultValue?: string
    ) {
        const value = this[property];
        let str = "";

        if (prefix) {
            prefix += " ";
        }

        if (
            suffix &&
            (!hasNumericSuffix ||
                isString(value) ||
                (!isNumberFinite(value) && this.allowANS))
        ) {
            suffix = " " + suffix;
        }

        if (isString(value)) {
            if (isConstant(value)) {
                str =
                    "`" +
                    (value.length > 1 ? value.toUpperCase() : value) +
                    "`";
            } else {
                if (!lookupVariable(this.calculator.block, value)?.label) {
                    return "";
                }

                str = "@" + value;
            }
        } else {
            str =
                isNumberFinite(value) || (!defaultValue && !this.allowANS)
                    ? (boldNumbers ? "**" : "") +
                      L10n.locale.number(
                          isNumberFinite(value) ? value : 0,
                          "auto",
                          false
                      ) +
                      (boldNumbers ? "**" : "")
                    : this.isFirst && this.isSubCalculation
                    ? "`ANS` " +
                      pgettext("block:calculator", "of parent calculation")
                    : this.isFirst && !defaultValue && this.calculator.firstANS
                    ? this.calculator.firstANS
                    : "`" + (defaultValue || "ANS") + "`";
        }

        return (prefix || "") + str + (suffix || "");
    }

    @editor
    defineEditor(): void {
        Operation.get(this.opcode)?.editor(this, this.editor);
    }

    @created
    initOperation() {
        if (this.opcode === "calc") {
            this.calculator.outcome?.hook(
                "OnSlotProperty",
                "synchronous",
                (calculator: Slots.ISlotPropertyEvent<Slots.Numeric>) => {
                    switch (calculator.property) {
                        case "decimal":
                        case "precision":
                        case "separator":
                        case "minimum":
                        case "maximum":
                        case "prefix":
                        case "prefixPlural":
                        case "suffix":
                        case "suffixPlural":
                            setAny(
                                this.outcome,
                                calculator.property,
                                calculator.slot[calculator.property]
                            );

                            break;
                    }
                },
                this
            );
        }
    }

    @deleted
    deleteOperation() {
        if (this.opcode === "calc") {
            this.calculator.outcome?.unhookContext(this);
            this.calculator.block.slots.delete(this.id, "dynamic");

            this.operations.each((operation) => operation.deleteOperation());
        }
    }
}
