/** Dependencies */
import { Collection, Markdown, pgettext } from "tripetto";
import { ICalculator } from "./interface";
import { Operation } from "./operation";
import { operatorMenu } from "./menu";

export const calculator = (
    ref: ICalculator | Operation,
    sMessage: string = pgettext(
        "block:calculator",
        "Initial value is the last outcome (ANS)."
    ),
    sBanner?: string
): Collection.IProperties<Operation, ICalculator> => ({
    collection: ref.operations,
    banner: sBanner,
    title: pgettext("block:calculator", "Operations"),
    icon: true,
    allowAutoSorting: false,
    allowCleanup: pgettext("block:calculator", "Remove invalid operations"),
    placeholder: pgettext("block:calculator", "Invalid operation"),
    allowVariables: true,
    allowFormatting: true,
    markdown:
        Markdown.MarkdownFeatures.Formatting |
        Markdown.MarkdownFeatures.InlineCode,
    showAliases: (operation) =>
        ref.block.slots.select(operation.id, "dynamic")?.alias,
    onReposition: (operation) => operation.refresh("name"),
    onResize:
        (ref instanceof Operation && (() => ref.refresh("name"))) || undefined,
    menu: () => operatorMenu(ref, ref instanceof Operation),
    emptyMessage: {
        message:
            ref instanceof Operation || !ref.startBlank
                ? "**" +
                  sMessage +
                  "**\n\n" +
                  pgettext(
                      "block:calculator",
                      "Click the + button to add an operation."
                  ) +
                  "\n\n" +
                  pgettext(
                      "block:calculator",
                      "Use **=** operation to set another initial value."
                  )
                : "**" +
                  pgettext(
                      "block:calculator",
                      "Every calculation begins with an initial value."
                  ) +
                  "**\n\n" +
                  pgettext(
                      "block:calculator",
                      "Click the + button to specify that value."
                  ) +
                  "\n\n" +
                  pgettext(
                      "block:calculator",
                      "After that you can add more operations."
                  ),
        height: 96,
    },
});
