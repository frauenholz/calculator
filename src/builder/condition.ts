/** Package information defined using webpack */
declare const PACKAGE_NAME: string;
declare const PACKAGE_VERSION: string;

/** Dependencies */
import {
    ConditionBlock,
    Forms,
    L10n,
    Slots,
    affects,
    definition,
    editor,
    isNumberFinite,
    isString,
    lookupVariable,
    pgettext,
    populateVariables,
    tripetto,
} from "tripetto";
import { TModes } from "../runner/modes";

/** Assets */
import ICON from "../../assets/icon.svg";

@tripetto({
    type: "condition",
    context: PACKAGE_NAME,
    identifier: PACKAGE_NAME,
    version: PACKAGE_VERSION,
    icon: ICON,
    get label() {
        return pgettext("block:calculator", "Verify calculation");
    },
})
export class CalculatorCondition extends ConditionBlock {
    readonly allowMarkdown = true;

    @definition
    @affects("#name")
    mode: TModes = "equal";

    @definition
    @affects("#name")
    value?: number | string;

    @definition
    @affects("#name")
    to?: number | string;

    // Return an empty label, since the node name is in the block name already.
    get label() {
        return "";
    }

    get name() {
        const slot = this.slot;

        if (slot instanceof Slots.Numeric) {
            const value = this.parse(slot, this.value);

            switch (this.mode) {
                case "between":
                    return `${value} ≤ @${slot.id} ≤ ${this.parse(
                        slot,
                        this.to
                    )}`;
                case "not-between":
                    return `@${slot.id} < ${value} ${pgettext(
                        "block:calculator",
                        "or"
                    )} @${slot.id} > ${this.parse(slot, this.to)}`;
                case "defined":
                    return `@${slot.id} ${pgettext(
                        "block:calculator",
                        "calculated"
                    )}`;
                case "undefined":
                    return `@${slot.id} ${pgettext(
                        "block:calculator",
                        "not calculated"
                    )}`;
                case "not-equal":
                    return `@${slot.id} \u2260 ${value}`;
                case "above":
                case "below":
                case "equal":
                    return `@${slot.id} ${
                        this.mode === "above"
                            ? ">"
                            : this.mode === "below"
                            ? "<"
                            : "="
                    } ${value}`;
            }
        }

        return this.type.label;
    }

    get title() {
        return this.slot?.label || this.node?.label;
    }

    private parse(
        slot: Slots.Numeric,
        value: number | string | undefined
    ): string {
        if (isNumberFinite(value)) {
            return slot.toString(value, (n, p) =>
                L10n.locale.number(n, p, false)
            );
        } else if (
            isString(value) &&
            value &&
            lookupVariable(this, value)?.label
        ) {
            return `@${value}`;
        }

        return "\\_\\_";
    }

    @editor
    defineEditor(): void {
        this.editor.form({
            title: pgettext("block:calculator", "Compare mode"),
            controls: [
                new Forms.Radiobutton<TModes>(
                    [
                        {
                            label: pgettext(
                                "block:calculator",
                                "Calculation is equal to"
                            ),
                            value: "equal",
                        },
                        {
                            label: pgettext(
                                "block:calculator",
                                "Calculation is not equal to"
                            ),
                            value: "not-equal",
                        },
                        {
                            label: pgettext(
                                "block:calculator",
                                "Calculation is lower than"
                            ),
                            value: "below",
                        },
                        {
                            label: pgettext(
                                "block:calculator",
                                "Calculation is higher than"
                            ),
                            value: "above",
                        },
                        {
                            label: pgettext(
                                "block:calculator",
                                "Calculation is between"
                            ),
                            value: "between",
                        },
                        {
                            label: pgettext(
                                "block:calculator",
                                "Calculation is not between"
                            ),
                            value: "not-between",
                        },
                        {
                            label: pgettext(
                                "block:calculator",
                                "Calculation is valid"
                            ),
                            value: "defined",
                        },
                        {
                            label: pgettext(
                                "block:calculator",
                                "Calculation is not valid"
                            ),
                            value: "undefined",
                        },
                    ],
                    Forms.Radiobutton.bind(this, "mode", "equal")
                ).on((mode: Forms.Radiobutton<TModes>) => {
                    from.visible(
                        mode.value !== "defined" && mode.value !== "undefined"
                    );
                    to.visible(
                        mode.value === "between" || mode.value === "not-between"
                    );

                    switch (mode.value) {
                        case "equal":
                            from.title = pgettext(
                                "block:calculator",
                                "If calculation equals"
                            );
                            break;
                        case "not-equal":
                            from.title = pgettext(
                                "block:calculator",
                                "If calculation not equals"
                            );
                            break;
                        case "below":
                            from.title = pgettext(
                                "block:calculator",
                                "If calculation is lower than"
                            );
                            break;
                        case "above":
                            from.title = pgettext(
                                "block:calculator",
                                "If calculation is higher than"
                            );
                            break;
                        case "between":
                            from.title = pgettext(
                                "block:calculator",
                                "If calculation is between"
                            );
                            break;
                        case "not-between":
                            from.title = pgettext(
                                "block:calculator",
                                "If calculation is not between"
                            );
                            break;
                    }
                }),
            ],
        });

        const addCondition = (
            property: "value" | "to",
            title: string,
            visible: boolean
        ) => {
            const value = this[property];
            const src = this.slot as Slots.Numeric | undefined;
            const numberControl = new Forms.Numeric(
                isNumberFinite(value) ? value : 0
            )
                .label(pgettext("block:calculator", "Use fixed number"))
                .precision(src?.precision || 0)
                .digits(src?.digits || 0)
                .decimalSign(src?.decimal || "")
                .thousands(src?.separator ? true : false, src?.separator || "")
                .prefix(src?.prefix || "")
                .prefixPlural(src?.prefixPlural || undefined)
                .suffix(src?.suffix || "")
                .suffixPlural(src?.suffixPlural || undefined)
                .min(src?.minimum)
                .max(src?.maximum)
                .autoFocus(property === "value")
                .escape(this.editor.close)
                .enter(
                    () =>
                        ((this.mode !== "between" &&
                            this.mode !== "not-between") ||
                            property === "to") &&
                        this.editor.close()
                )
                .on((input) => {
                    if (input.isFormVisible && input.isObservable) {
                        this[property] = input.value;
                    }
                });

            const variables = populateVariables(
                this,
                (slot) =>
                    slot instanceof Slots.Number ||
                    slot instanceof Slots.Numeric,
                isString(value) ? value : undefined,
                true,
                this.slot?.id
            );
            const variableControl = new Forms.Dropdown(
                variables,
                isString(value) ? value : ""
            )
                .label(pgettext("block:calculator", "Use value of"))
                .width("full")
                .on((variable) => {
                    if (variable.isFormVisible && variable.isObservable) {
                        this[property] = variable.value || "";
                    }
                });

            return this.editor
                .form({
                    title,
                    controls: [
                        new Forms.Radiobutton<"number" | "variable">(
                            [
                                {
                                    label: pgettext(
                                        "block:calculator",
                                        "Number"
                                    ),
                                    value: "number",
                                },
                                {
                                    label: pgettext(
                                        "block:calculator",
                                        "Value"
                                    ),
                                    value: "variable",
                                    disabled: variables.length === 0,
                                },
                            ],
                            isString(value) ? "variable" : "number"
                        ).on((type) => {
                            numberControl.visible(type.value === "number");
                            variableControl.visible(type.value === "variable");

                            if (numberControl.isObservable) {
                                numberControl.focus();
                            }
                        }),
                        numberControl,
                        variableControl,
                    ],
                })
                .visible(visible);
        };

        const from = addCondition(
            "value",
            pgettext("block:calculator", "If calculation equals"),
            this.mode !== "defined" && this.mode !== "undefined"
        );
        const to = addCondition(
            "to",
            pgettext("block:calculator", "And"),
            this.mode === "between" || this.mode === "not-between"
        );
    }
}
