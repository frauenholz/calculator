import { pgettext } from "tripetto";
import { IOperation } from "../operation";
import { controls } from "../controls";

export const gammaOperation: IOperation = {
    name: (operation) => operation.format("value", "*gamma* (", ")"),
    title: () => pgettext("block:calculator", "Gamma"),
    editor: (operation, editor) => {
        controls.explanation(
            editor,
            pgettext(
                "block:calculator",
                "Calculates the gamma of a positive number ([learn more](https://en.wikipedia.org/wiki/Gamma_function))."
            )
        );

        controls.type(operation, editor, true, true);
    },
};
