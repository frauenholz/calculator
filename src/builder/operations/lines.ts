import { pgettext } from "tripetto";
import { IOperation } from "../operation";
import { controls } from "../controls";

export const linesOperation: IOperation = {
    name: (operation) =>
        operation.format(
            "value",
            `*${pgettext("block:calculator", "lines")}* (`,
            ")"
        ),
    title: () => pgettext("block:calculator", "Line count"),
    editor: (operation, editor) => {
        controls.explanation(
            editor,
            pgettext(
                "block:calculator",
                "Counts the number of lines in a text."
            )
        );
    },
};
