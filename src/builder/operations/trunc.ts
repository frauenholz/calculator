import { pgettext } from "tripetto";
import { IOperation } from "../operation";
import { controls } from "../controls";

export const truncOperation: IOperation = {
    name: (operation) => operation.format("value", `*trunc* (`, ")"),
    title: () => pgettext("block:calculator", "Trunc"),
    editor: (operation, editor) => {
        controls.explanation(
            editor,
            pgettext(
                "block:calculator",
                "Removes decimals from a floating point number."
            )
        );

        controls.type(operation, editor, true, true);
    },
};
