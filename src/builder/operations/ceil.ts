import { pgettext } from "tripetto";
import { IOperation } from "../operation";
import { controls } from "../controls";

export const ceilOperation: IOperation = {
    name: (operation) => operation.format("value", "\u2308", "\u2309"),
    title: () => pgettext("block:calculator", "Ceil"),
    editor: (operation, editor) => {
        controls.explanation(
            editor,
            pgettext(
                "block:calculator",
                "Rounds a floating point number up ([learn more](https://en.wikipedia.org/wiki/Floor_and_ceiling_functions))."
            )
        );

        controls.type(operation, editor, true, true);
    },
};
