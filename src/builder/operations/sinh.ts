import { pgettext } from "tripetto";
import { IOperation } from "../operation";
import { controls } from "../controls";

export const sinhOperation: IOperation = {
    name: (operation) => operation.format("value", "*sinh* (", ")"),
    title: () => pgettext("block:calculator", "Hyperbolic sine"),
    editor: (operation, editor) => {
        controls.explanation(
            editor,
            pgettext(
                "block:calculator",
                "Calculates the hyperbolic sine of the given hyperbolic angle ([learn more](https://en.wikipedia.org/wiki/Hyperbolic_functions))."
            )
        );

        controls.type(operation, editor);
    },
};
