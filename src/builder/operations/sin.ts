import { pgettext } from "tripetto";
import { IOperation } from "../operation";
import { controls } from "../controls";

export const sinOperation: IOperation = {
    name: (operation) =>
        operation.format(
            "value",
            "*sin* (",
            (operation.angleUnits === "radians"
                ? "rad"
                : operation.angleUnits === "gradians"
                ? "ᵍ"
                : "°") + " )",
            operation.angleUnits !== "radians"
        ),
    title: () => pgettext("block:calculator", "Sine"),
    editor: (operation, editor) => {
        controls.explanation(
            editor,
            pgettext(
                "block:calculator",
                "Calculates the sine of the given angle ([learn more](https://en.wikipedia.org/wiki/Trigonometric_functions)). The result is a number between `-1` and `1`."
            )
        );

        const [, numberControl] = controls.type(operation, editor);

        controls.angles(operation, editor, (angles) =>
            numberControl.suffix(
                angles === "radians"
                    ? " rad"
                    : angles === "gradians"
                    ? "ᵍ"
                    : "°"
            )
        );
    },
};
