import { pgettext } from "tripetto";
import { IOperation } from "../operation";
import { controls } from "../controls";

export const acoshOperation: IOperation = {
    name: (operation) => operation.format("value", "*cosh⁻¹* (", ")"),
    title: () => pgettext("block:calculator", "Inverse hyperbolic cosine"),
    editor: (operation, editor) => {
        controls.explanation(
            editor,
            pgettext(
                "block:calculator",
                "Calculates the inverse hyperbolic cosine (arccosine) of the given number ([learn more](https://en.wikipedia.org/wiki/Inverse_hyperbolic_functions))."
            )
        );

        controls.type(operation, editor);
    },
};
