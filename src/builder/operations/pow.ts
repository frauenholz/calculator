import { pgettext } from "tripetto";
import { IOperation } from "../operation";
import { controls } from "../controls";

export const powOperation: IOperation = {
    name: (operation) => {
        const a = operation.format("value", "", "^ ");
        const b = operation.format("exponent");

        return a && b ? a + b : "";
    },
    title: () => pgettext("block:calculator", "Power"),
    editor: (operation, editor) => {
        controls.explanation(
            editor,
            pgettext(
                "block:calculator",
                "Calculates the base to the power of the given exponent ([learn more](https://en.wikipedia.org/wiki/Exponentiation))."
            )
        );

        controls.type(
            operation,
            editor,
            true,
            true,
            true,
            "value",
            false,
            true,
            pgettext("block:calculator", "Base")
        );

        controls.type(
            operation,
            editor,
            true,
            true,
            true,
            "exponent",
            false,
            false,
            pgettext("block:calculator", "Exponent")
        );
    },
};
