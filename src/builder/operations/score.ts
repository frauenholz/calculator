import {
    Collection,
    Forms,
    Markdown,
    NodeBlock,
    Slots,
    castToString,
    each,
    getMetadata,
    isNumberFinite,
    isString,
    lookupVariable,
    makeMarkdownSafe,
    pgettext,
    set,
} from "tripetto";
import { IOperation } from "../operation";
import { controls } from "../controls";
import { ICalculatorMetadata } from "../metadata";

export const scoreOperation: IOperation = {
    name: (operation) =>
        operation.format(
            "value",
            `*${pgettext("block:calculator", "score")}* (`,
            ")"
        ),
    title: (operation) =>
        (isString(operation.value)
            ? lookupVariable(operation.calculator.block, operation.value)?.label
            : "") || pgettext("block:calculator", "Score"),
    editor: (operation, editor) => {
        const variable =
            isString(operation.value) &&
            lookupVariable(operation.calculator.block, operation.value);

        controls.explanation(
            editor,
            pgettext(
                "block:calculator",
                "Calculates a score based on the score list below."
            )
        );

        if (variable && variable.block instanceof NodeBlock) {
            const slot = variable.slot || variable.pipe;

            if (slot) {
                const scoreControls: Forms.Numeric[] = [];

                const addScore = (
                    id: string,
                    name: string | undefined,
                    alias: string | undefined,
                    initial: number | undefined
                ) => {
                    alias = alias || "";

                    if (id && (name || alias)) {
                        const score = operation.scores && operation.scores[id];

                        scoreControls.push(
                            new Forms.Numeric(
                                isNumberFinite(score) ? score : initial
                            )
                                .precision("auto")
                                .thousands(false)
                                .markdown({
                                    features:
                                        Markdown.MarkdownFeatures.Formatting,
                                })
                                .label(
                                    name && alias
                                        ? `**${name}** *${alias}*`
                                        : name || alias
                                )
                                .on((scoreValue) =>
                                    setScore(id, scoreValue.value)
                                )
                        );
                    }
                };

                const setScore = (id: string, score: number) => {
                    if (!operation.scores) {
                        operation.scores = {};
                    }

                    if (operation.scores[id] !== score) {
                        set(operation.scores, id, score);

                        operation.calculator.block.detectChange();
                    }
                };

                const supply = Collection.find(variable);

                if (supply) {
                    supply.collection.each((score) =>
                        addScore(
                            score.id,
                            score.labelWithoutMarkdown,
                            makeMarkdownSafe(score.getAliasOfItem()),
                            score.getScoreOfItem()
                        )
                    );
                } else {
                    const metadata = getMetadata<ICalculatorMetadata>(
                        variable.block,
                        "calculator"
                    );

                    if (metadata) {
                        each(
                            (metadata[slot.reference] || metadata["*"])?.scores,
                            (score) =>
                                addScore(
                                    castToString(score.reference),
                                    score.label ||
                                        (slot instanceof Slots.Boolean
                                            ? score.reference === true
                                                ? slot.labelForTrue ||
                                                  pgettext(
                                                      "block:calculator",
                                                      "True"
                                                  )
                                                : score.reference === false
                                                ? slot.labelForFalse ||
                                                  pgettext(
                                                      "block:calculator",
                                                      "False"
                                                  )
                                                : ""
                                            : undefined),
                                    score.alias,
                                    score.score
                                )
                        );
                    }
                }

                if (scoreControls.length) {
                    scoreControls[0].autoFocus();
                    scoreControls[scoreControls.length - 1].enter(editor.close);
                    scoreControls[scoreControls.length - 1].escape(
                        editor.close
                    );
                }

                editor.form({
                    title: pgettext("block:calculator", "Scores"),
                    controls: scoreControls.length
                        ? scoreControls
                        : [
                              new Forms.Notification(
                                  pgettext(
                                      "block:calculator",
                                      "There are no scorable items found for this block. Make sure to add items to the block first and then return here to set their scores."
                                  ),
                                  "info"
                              ),
                          ],
                });
            }
        }
    },
};
