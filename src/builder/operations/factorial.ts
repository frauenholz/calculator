import { pgettext } from "tripetto";
import { IOperation } from "../operation";
import { controls } from "../controls";

export const factorialOperation: IOperation = {
    name: (operation) => operation.format("value", undefined, "!", true),
    title: () => "n!",
    editor: (operation, editor) => {
        controls.explanation(
            editor,
            pgettext(
                "block:calculator",
                "Calculates the factorial of a positive number ([learn more](https://en.wikipedia.org/wiki/Factorial))."
            )
        );

        controls.type(operation, editor, true, true);
    },
};
