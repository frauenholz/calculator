import { pgettext } from "tripetto";
import { IOperation } from "../operation";
import { controls } from "../controls";

export const asinhOperation: IOperation = {
    name: (operation) => operation.format("value", "*sinh⁻¹* (", ")"),
    title: () => pgettext("block:calculator", "Inverse hyperbolic sine"),
    editor: (operation, editor) => {
        controls.explanation(
            editor,
            pgettext(
                "block:calculator",
                "Calculates the inverse hyperbolic sine (arcsine) of the given number ([learn more](https://en.wikipedia.org/wiki/Inverse_hyperbolic_functions))."
            )
        );

        controls.type(operation, editor);
    },
};
