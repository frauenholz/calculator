import { pgettext } from "tripetto";
import { IOperation } from "../operation";
import { controls } from "../controls";

export const coshOperation: IOperation = {
    name: (operation) => operation.format("value", "*cosh* (", ")"),
    title: () => pgettext("block:calculator", "Hyperbolic cosine"),
    editor: (operation, editor) => {
        controls.explanation(
            editor,
            pgettext(
                "block:calculator",
                "Calculates the hyperbolic cosine of the given hyperbolic angle ([learn more](https://en.wikipedia.org/wiki/Hyperbolic_functions))."
            )
        );

        controls.type(operation, editor);
    },
};
