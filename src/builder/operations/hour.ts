import { isString, lookupVariable, pgettext } from "tripetto";
import { IOperation } from "../operation";
import { controls } from "../controls";

export const hourOperation: IOperation = {
    name: (operation) =>
        operation.format(
            "value",
            `*${pgettext("block:calculator", "hour")}* (`,
            ")"
        ),
    title: (operation) =>
        (isString(operation.value)
            ? lookupVariable(operation.calculator.block, operation.value)?.label
            : "") || pgettext("block:calculator", "Hour"),
    editor: (operation, editor) => {
        controls.explanation(
            editor,
            pgettext(
                "block:calculator",
                "Retrieves a number between 0 and 23, representing the hours of the date/time."
            )
        );
    },
};
