import { pgettext } from "tripetto";
import { IOperation } from "../operation";
import { controls } from "../controls";

export const tanhOperation: IOperation = {
    name: (operation) => operation.format("value", "*tanh* (", ")"),
    title: () => pgettext("block:calculator", "Hyperbolic tangent"),
    editor: (operation, editor) => {
        controls.explanation(
            editor,
            pgettext(
                "block:calculator",
                "Calculates the hyperbolic tangent of the given hyperbolic angle ([learn more](https://en.wikipedia.org/wiki/Hyperbolic_functions))."
            )
        );

        controls.type(operation, editor);
    },
};
