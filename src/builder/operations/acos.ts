import { pgettext } from "tripetto";
import { IOperation } from "../operation";
import { controls } from "../controls";

export const acosOperation: IOperation = {
    name: (operation) =>
        operation.format(
            "value",
            "*cos⁻¹* (",
            ") " +
                (operation.angleUnits === "radians"
                    ? "rad"
                    : operation.angleUnits === "gradians"
                    ? "ᵍ"
                    : "°")
        ),
    title: () => pgettext("block:calculator", "Inverse cosine (arccosine)"),
    editor: (operation, editor) => {
        controls.explanation(
            editor,
            pgettext(
                "block:calculator",
                "Calculates the inverse cosine (arccosine) of the given number ([learn more](https://en.wikipedia.org/wiki/Inverse_trigonometric_functions)). This number should be a value between `-1` and `1`."
            )
        );

        controls.type(operation, editor)[1].min(-1).max(1);
        controls.angles(operation, editor);
    },
};
