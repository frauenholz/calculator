import { Forms, Slots, makeMarkdownSafe, npgettext, pgettext } from "tripetto";
import { IOperation } from "../operation";
import { controls } from "../controls";
import { calculator } from "../collection";
import { Calculator } from "../calculator";

export const calculatorOperation: IOperation = {
    name: (operation) =>
        `${
            makeMarkdownSafe(operation.description || "") ||
            pgettext("block:calculator", "Calculation")
        } *(${npgettext(
            "block:calculator",
            "%1 operation",
            "%1 operations",
            operation.operations.count
        )})*`,
    title: (operation) =>
        operation.description || pgettext("block:calculator", "Subcalculation"),
    editor: (operation, editor) => {
        controls.explanation(
            editor,
            pgettext(
                "block:calculator",
                "Performs a subcalculation. The initial value for this subcalculation is automatically set to the current outcome (**ANS**) of the parent calculation. If you want to begin this subcalculation with another value, then add an equal (**=**) operation to begin with."
            )
        );

        editor.collection(calculator(operation));
        editor.form({
            title: pgettext("block:calculator", "Description"),
            controls: [
                new Forms.Text(
                    "singleline",
                    Forms.Text.bind(operation, "description", undefined)
                )
                    .autoFocus(operation.operations.count === 0)
                    .on((description) => {
                        if (operation.outcome) {
                            operation.outcome.label = description.value;
                        }

                        exportability.visible(
                            description.value &&
                                operation.block instanceof Calculator
                                ? true
                                : false
                        );
                    }),
            ],
        });

        const slotAliasEnabled = new Forms.Checkbox(
            pgettext(
                "block:calculator",
                "Set an alias for this subcalculation"
            ),
            operation.outcome?.alias ? true : false
        ).on((aliasEnabled) => slotAlias.visible(aliasEnabled.isChecked));

        const slotAlias = new Forms.Group([
            new Forms.Text("singleline", operation.outcome?.alias || "")
                .placeholder(
                    pgettext(
                        "block:calculator",
                        "Type alias identifier here..."
                    )
                )
                .indent(32)
                .on((alias) => {
                    if (operation.outcome) {
                        const current = operation.outcome.alias;

                        operation.outcome.alias =
                            (alias.isObservable && alias.value) || undefined;

                        if (operation.outcome.alias !== current) {
                            operation.refresh("name");
                        }
                    }
                }),
            new Forms.Static(
                pgettext(
                    "block:calculator",
                    "This alias will be used as identifier of the subcalculation in the dataset."
                )
            ).indent(32),
        ]).visible(operation.outcome?.alias ? true : false);

        const slotOptions = new Forms.Group([
            new Forms.Checkbox(
                pgettext(
                    "block:calculator",
                    "Make outcome exportable and include it in the dataset"
                ),
                operation.outcome?.exportable || false
            ).on((exportable) => {
                if (operation.outcome) {
                    operation.outcome.exportable = exportable.isChecked;
                }
            }),
            slotAliasEnabled,
        ]).visible(operation.outcome ? true : false);

        const exportability = editor
            .form({
                title: pgettext("block:calculator", "Exportability"),
                controls: [
                    new Forms.Checkbox(
                        pgettext(
                            "block:calculator",
                            "Make outcome available for other blocks"
                        ),
                        operation.outcome ? true : false
                    ).on((slot) => {
                        if (
                            slot.isFormVisible &&
                            slot.isObservable &&
                            slot.isChecked &&
                            operation.description &&
                            operation.block instanceof Calculator
                        ) {
                            const calculatorSlot =
                                operation.calculator.block.slots.dynamic({
                                    type: Slots.Numeric,
                                    reference: operation.id,
                                    label: operation.description,
                                    exportable: false,
                                });

                            if (operation.calculator.outcome) {
                                calculatorSlot.decimal =
                                    operation.calculator.outcome.decimal;
                                calculatorSlot.precision =
                                    operation.calculator.outcome.precision;
                                calculatorSlot.digits =
                                    operation.calculator.outcome.digits;
                                calculatorSlot.separator =
                                    operation.calculator.outcome.separator;
                                calculatorSlot.minimum =
                                    operation.calculator.outcome.minimum;
                                calculatorSlot.maximum =
                                    operation.calculator.outcome.maximum;
                                calculatorSlot.prefix =
                                    operation.calculator.outcome.prefix;
                                calculatorSlot.prefixPlural =
                                    operation.calculator.outcome.prefixPlural;
                                calculatorSlot.suffix =
                                    operation.calculator.outcome.suffix;
                                calculatorSlot.suffixPlural =
                                    operation.calculator.outcome.suffixPlural;
                            }
                        } else {
                            operation.calculator.block.slots.delete(
                                operation.id,
                                "dynamic"
                            );
                        }

                        slotOptions.visible(slot.isChecked);
                        slotAlias.visible(
                            slot.isChecked && slotAliasEnabled.isChecked
                        );

                        operation.refresh("name");
                    }),
                    slotOptions,
                    slotAlias,
                ],
            })
            .visible(
                operation.description && operation.block instanceof Calculator
                    ? true
                    : false
            );
    },
};
