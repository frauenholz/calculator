import { pgettext } from "tripetto";
import { IOperation } from "../operation";
import { controls } from "../controls";

export const atanOperation: IOperation = {
    name: (operation) =>
        operation.format(
            "value",
            "*tan⁻¹* (",
            ") " +
                (operation.angleUnits === "radians"
                    ? "rad"
                    : operation.angleUnits === "gradians"
                    ? "ᵍ"
                    : "°")
        ),
    title: () => pgettext("block:calculator", "Inverse tangent (arctangent)"),
    editor: (operation, editor) => {
        controls.explanation(
            editor,
            pgettext(
                "block:calculator",
                "Calculates the inverse tangent (arctangent) of the given number ([learn more](https://en.wikipedia.org/wiki/Inverse_trigonometric_functions))."
            )
        );

        controls.type(operation, editor);
        controls.angles(operation, editor);
    },
};
