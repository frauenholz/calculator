import { pgettext } from "tripetto";
import { IOperation } from "../operation";
import { controls } from "../controls";

export const floorOperation: IOperation = {
    name: (operation) => operation.format("value", "\u230a", "\u230b"),
    title: () => pgettext("block:calculator", "Floor"),
    editor: (operation, editor) => {
        controls.explanation(
            editor,
            pgettext(
                "block:calculator",
                "Rounds a floating point number down ([learn more](https://en.wikipedia.org/wiki/Floor_and_ceiling_functions))."
            )
        );

        controls.type(operation, editor, true, true);
    },
};
