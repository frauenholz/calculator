import {
    DateTime,
    Forms,
    Slots,
    isNumberFinite,
    isString,
    lookupVariable,
    pgettext,
} from "tripetto";
import { IOperation } from "../operation";
import { controls } from "../controls";
import { TAge } from "../../runner/age";

function getAgeIn(ageIn: TAge = "years") {
    switch (ageIn) {
        case "months":
            return pgettext("block:calculator", "Months");
        case "days":
            return pgettext("block:calculator", "Days");
        case "hours":
            return pgettext("block:calculator", "Hours");
        case "minutes":
            return pgettext("block:calculator", "Minutes");
        case "seconds":
            return pgettext("block:calculator", "Seconds");
        case "milliseconds":
            return pgettext("block:calculator", "Milliseconds");
        default:
            return pgettext("block:calculator", "Years");
    }
}

export const ageOperation: IOperation = {
    name: (operation) =>
        operation.format(
            "value",
            `*${pgettext("block:calculator", "age")}* (`,
            ") " + getAgeIn(operation.ageIn).toLowerCase()
        ),
    title: (operation) =>
        (isString(operation.value) &&
            lookupVariable(operation.calculator.block, operation.value)
                ?.label) ||
        pgettext("block:calculator", "Age"),
    editor: (operation, editor) => {
        const variable =
            isString(operation.value) &&
            lookupVariable(operation.calculator.block, operation.value);
        const supportsTime =
            (variable &&
                variable.slot instanceof Slots.Date &&
                variable.slot.supportsTime) ||
            false;
        const control = new Forms.Radiobutton<"current" | "variable" | "date">(
            [
                {
                    label: pgettext("block:calculator", "Current date"),
                    value: "current",
                },
                {
                    label: pgettext("block:calculator", "Fixed date"),
                    value: "date",
                },
                {
                    label: pgettext("block:calculator", "Value"),
                    value: "variable",
                },
            ],
            isString(operation.reference)
                ? "variable"
                : isNumberFinite(operation.reference)
                ? "date"
                : "current"
        ).on((type) => {
            dateForm.visible(type.value === "date");
            variableForm[0].visible(type.value === "variable");

            if (type.value === "current") {
                operation.reference = undefined;
            }
        });

        controls.explanation(
            editor,
            pgettext(
                "block:calculator",
                "Calculates the age (time) between two points in time."
            )
        );

        editor.form({
            title: pgettext("block:calculator", "Calculate age using"),
            controls: [control],
        });

        const dateForm = editor.form({
            title: pgettext("block:calculator", "Use fixed date"),
            controls: [
                new Forms.DateTime(
                    isNumberFinite(operation.reference)
                        ? operation.reference
                        : DateTime.UTC
                )
                    .zone("UTC")
                    .years(
                        new Date().getFullYear() - 150,
                        new Date().getFullYear() + 50
                    )
                    .features(
                        Forms.DateTimeFeatures.Date |
                            (supportsTime
                                ? Forms.DateTimeFeatures.TimeHoursAndMinutesOnly
                                : Forms.DateTimeFeatures.Weekday)
                    )
                    .required()
                    .on((input) => {
                        if (input.isFormVisible && input.isObservable) {
                            operation.reference = input.value;
                        }
                    }),
            ],
        });

        const variableForm = controls.variable(
            operation,
            editor,
            "reference",
            "form",
            true,
            (slot) => slot instanceof Slots.Date && slot.id !== operation.value
        );

        control.buttonDisabled("variable", variableForm[1] === 0);

        editor.form({
            title: pgettext("block:calculator", "Units"),
            controls: [
                new Forms.Radiobutton<TAge>(
                    (
                        [
                            "years",
                            "months",
                            "days",
                            "hours",
                            "minutes",
                            "seconds",
                            "milliseconds",
                        ] as TAge[]
                    ).map((unit) => ({
                        label: getAgeIn(unit),
                        value: unit,
                    })),
                    Forms.Radiobutton.bind(
                        operation,
                        "ageIn",
                        undefined,
                        "years"
                    )
                ),
            ],
        });

        editor.form({
            title: pgettext("block:calculator", "Options"),
            controls: [
                new Forms.Checkbox(
                    pgettext(
                        "block:calculator",
                        "Always return a positive (absolute) value"
                    ),
                    Forms.Checkbox.bind(operation, "ageAbs", undefined)
                ).description(
                    pgettext(
                        "block:calculator",
                        "If unchecked, a negative value is possible, which indicates that the source date is after the date that serves as compare date for the calculation."
                    )
                ),
            ],
        });
    },
};
