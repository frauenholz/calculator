import { Forms, Slots, isString, pgettext } from "tripetto";
import { IOperation } from "../operation";
import { controls } from "../controls";
import { TModes } from "../../runner/modes";
import { isConstant } from "../../runner/constants";

function isVariable(value: string | number | undefined) {
    return isString(value) && !isConstant(value);
}

export const equationOperation: IOperation = {
    name: (operation) => {
        const value = operation.format(
            "value",
            undefined,
            undefined,
            undefined,
            true
        );
        const cona = operation.format(
            "cona",
            undefined,
            undefined,
            undefined,
            true
        );
        const conb = operation.format(
            "conb",
            undefined,
            undefined,
            undefined,
            true
        );
        const outa = operation.format(
            "outa",
            undefined,
            undefined,
            undefined,
            true
        );
        const outb = operation.format(
            "outb",
            undefined,
            undefined,
            undefined,
            true
        );

        if (!value || !outa || !outb) {
            return "";
        }

        const out = `then ${outa} else ${outb}`;

        switch (operation.compareMode || "equal") {
            case "defined":
                return `${value} ${pgettext(
                    "block:calculator",
                    "not empty"
                )} ${out}`;
            case "between":
                return (
                    (cona && conb && `${cona} ≤ ${value} ≤ ${conb} ${out}`) ||
                    ""
                );
            case "above":
            case "below":
            case "equal":
                return (
                    (cona &&
                        `${value} ${
                            operation.compareMode === "above"
                                ? ">"
                                : operation.compareMode === "below"
                                ? "<"
                                : "="
                        } ${cona} ${out}`) ||
                    ""
                );
        }
    },
    title: () => pgettext("block:calculator", "Compare"),
    editor: (operation, editor) => {
        const compareFunction = new Forms.Radiobutton<TModes>(
            [
                {
                    label: pgettext("block:calculator", "Input is equal to"),
                    value: "equal",
                },
                {
                    label: pgettext("block:calculator", "Input is lower than"),
                    value: "below",
                },
                {
                    label: pgettext("block:calculator", "Input is higher than"),
                    value: "above",
                },
                {
                    label: pgettext("block:calculator", "Input is between"),
                    value: "between",
                },
                {
                    label: pgettext("block:calculator", "Input is not empty"),
                    value: "defined",
                    visible: isVariable(operation.value),
                },
            ],
            operation.compareMode || "equal"
        ).on((mode) => {
            operation.compareMode = mode.value || "equal";

            conditionA.visible(operation.compareMode !== "defined");
            conditionB.visible(mode.value === "between");

            switch (operation.compareMode) {
                case "equal":
                    conditionA.title = pgettext(
                        "block:calculator",
                        "If input equals"
                    );
                    break;
                case "below":
                    conditionA.title = pgettext(
                        "block:calculator",
                        "If input is lower than"
                    );
                    break;
                case "above":
                    conditionA.title = pgettext(
                        "block:calculator",
                        "If input is higher than"
                    );
                    break;
                case "between":
                    conditionA.title = pgettext(
                        "block:calculator",
                        "If input is between"
                    );
                    break;
            }
        });

        controls.explanation(
            editor,
            pgettext(
                "block:calculator",
                "Compares the supplied input and outputs a value based on the result of the comparison."
            )
        );

        controls.type(
            operation,
            editor,
            true,
            false,
            false,
            "value",
            false,
            true,
            pgettext("block:calculator", "Input"),
            (type: "number" | "ans" | "variable" | "constant", variable) => {
                compareFunction.buttonVisible("defined", type === "variable");

                if (conditionInputA && conditionInputB) {
                    const fnUpdate = (numberControl: Forms.Numeric) => {
                        numberControl.precision(
                            variable?.slot instanceof Slots.Numeric
                                ? variable.slot.precision || 0
                                : "auto"
                        );
                        numberControl.digits(
                            (variable?.slot instanceof Slots.Numeric &&
                                variable.slot.digits) ||
                                0
                        );
                        numberControl.decimalSign(
                            (variable?.slot instanceof Slots.Numeric &&
                                variable.slot.decimal) ||
                                ""
                        );
                        numberControl.thousands(
                            variable?.slot instanceof Slots.Numeric &&
                                variable.slot.separator
                                ? true
                                : false,
                            (variable?.slot instanceof Slots.Numeric &&
                                variable.slot.separator) ||
                                ""
                        );
                        numberControl.min(
                            (variable?.slot instanceof Slots.Numeric &&
                                variable.slot.minimum) ||
                                undefined
                        );
                        numberControl.max(
                            (variable?.slot instanceof Slots.Numeric &&
                                variable.slot.maximum) ||
                                undefined
                        );
                        numberControl.prefix(
                            (variable?.slot instanceof Slots.Numeric &&
                                variable.slot.prefix) ||
                                ""
                        );
                        numberControl.prefixPlural(
                            (variable?.slot instanceof Slots.Numeric &&
                                variable.slot.prefixPlural) ||
                                undefined
                        );
                        numberControl.suffix(
                            (variable?.slot instanceof Slots.Numeric &&
                                variable.slot.suffix) ||
                                ""
                        );
                        numberControl.suffixPlural(
                            (variable?.slot instanceof Slots.Numeric &&
                                variable.slot.suffixPlural) ||
                                undefined
                        );
                    };

                    fnUpdate(conditionInputA);
                    fnUpdate(conditionInputB);
                }
            }
        );

        editor.form({
            title: pgettext("block:calculator", "Compare mode"),
            controls: [compareFunction],
        });

        const [conditionA, conditionInputA] = controls.type(
            operation,
            editor,
            true,
            true,
            true,
            "cona",
            false,
            false,
            pgettext("block:calculator", "If input equals")
        );

        const [conditionB, conditionInputB] = controls.type(
            operation,
            editor,
            true,
            true,
            true,
            "conb",
            false,
            false,
            pgettext("block:calculator", "And")
        );

        conditionA.visible(operation.compareMode !== "defined");
        conditionB.visible(operation.compareMode === "between");

        controls.type(
            operation,
            editor,
            true,
            true,
            true,
            "outa",
            false,
            false,
            pgettext("block:calculator", "Then output")
        );

        controls.type(
            operation,
            editor,
            true,
            true,
            true,
            "outb",
            false,
            false,
            pgettext("block:calculator", "Else output")
        );
    },
};
