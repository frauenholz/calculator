import {
    Forms,
    Str,
    insertVariable,
    isFilledString,
    isString,
    lookupVariable,
    makeMarkdownSafe,
    pgettext,
} from "tripetto";
import { IOperation } from "../operation";
import { controls } from "../controls";
import { TModes } from "../../runner/modes";

export const evaluateOperation: IOperation = {
    name: (operation) => {
        const value = operation.format(
            "value",
            undefined,
            undefined,
            undefined,
            true
        );
        const outa = operation.format(
            "outa",
            undefined,
            undefined,
            undefined,
            true
        );
        const outb = operation.format(
            "outb",
            undefined,
            undefined,
            undefined,
            true
        );

        if (!value || !outa || !outb) {
            return "";
        }

        const out = `then ${outa} else ${outb}`;
        const match = isFilledString(operation.cona)
            ? `**${makeMarkdownSafe(
                  Str.replace(Str.replace(operation.cona, "\n", "↵"), " ", "⎵")
              )}**`
            : `*\\_\\_*`;

        switch (operation.compareMode || "equal") {
            case "defined":
                return `${value} ${pgettext(
                    "block:calculator",
                    "not empty"
                )} ${out}`;
            case "equal":
                return `${value} = ${match} ${out}`;
            case "contains":
                return `${value} ${pgettext(
                    "block:calculator",
                    "contains"
                )} ${match} ${out}`;
            case "starts":
                return `${value} ${pgettext(
                    "block:calculator",
                    "starts with"
                )} ${match} ${out}`;
            case "ends":
                return `${value} ${pgettext(
                    "block:calculator",
                    "ends with"
                )} ${match} ${out}`;
        }
    },
    title: (operation) =>
        (isString(operation.value) &&
            lookupVariable(operation.calculator.block, operation.value)
                ?.label) ||
        pgettext("block:calculator", "Text"),
    editor: (operation, editor) => {
        controls.explanation(
            editor,
            pgettext(
                "block:calculator",
                "Compares a text and outputs a value based on the result of the comparison."
            )
        );

        const group = new Forms.Group([
            new Forms.Text(
                "multiline",
                (isString(operation.cona) && operation.cona) || ""
            )
                .on((value) => {
                    operation.cona =
                        (value.isObservable && value.value) || undefined;
                })
                .sanitize(false)
                .action("@", insertVariable(operation.calculator.block))
                .autoFocus(),
            new Forms.Checkbox(
                pgettext("block:calculator", "Ignore case"),
                Forms.Checkbox.bind(operation, "ignoreCase", undefined, true)
            ),
        ]).visible(operation.compareMode !== "defined");

        editor.form({
            title: pgettext("block:calculator", "If text"),
            controls: [
                new Forms.Radiobutton<TModes>(
                    [
                        {
                            label: pgettext("block:calculator", "Matches"),
                            value: "equal",
                        },
                        {
                            label: pgettext("block:calculator", "Contains"),
                            value: "contains",
                        },
                        {
                            label: pgettext("block:calculator", "Starts with"),
                            value: "starts",
                        },
                        {
                            label: pgettext("block:calculator", "Ends with"),
                            value: "ends",
                        },
                        {
                            label: pgettext("block:calculator", "Is not empty"),
                            value: "defined",
                        },
                    ],
                    operation.compareMode || "equal"
                ).on((mode) => {
                    operation.compareMode = mode.value || "equal";

                    group.visible(operation.compareMode !== "defined");
                }),
                group,
            ],
        });

        controls.type(
            operation,
            editor,
            true,
            true,
            true,
            "outa",
            false,
            false,
            pgettext("block:calculator", "Then output")
        );

        controls.type(
            operation,
            editor,
            true,
            true,
            true,
            "outb",
            false,
            false,
            pgettext("block:calculator", "Else output")
        );
    },
};
