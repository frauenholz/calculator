import { pgettext } from "tripetto";
import { IOperation } from "../operation";
import { controls } from "../controls";

export const charsOperation: IOperation = {
    name: (operation) =>
        operation.format(
            "value",
            `*${pgettext("block:calculator", "characters")}* (`,
            ")"
        ),
    title: () => pgettext("block:calculator", "Character count"),
    editor: (operation, editor) => {
        controls.explanation(
            editor,
            pgettext(
                "block:calculator",
                "Counts the number of characters in a text."
            )
        );
    },
};
