import { pgettext } from "tripetto";
import { IOperation } from "../operation";
import { controls } from "../controls";

export const squareOperation: IOperation = {
    name: (operation) => operation.format("value", undefined, "²", true),
    title: () => "x²",
    editor: (operation, editor) => {
        controls.explanation(
            editor,
            pgettext(
                "block:calculator",
                "Multiplies the given input by itself ([learn more](https://en.wikipedia.org/wiki/Square_%28algebra%29))."
            )
        );

        controls.type(operation, editor, true, true);
    },
};
