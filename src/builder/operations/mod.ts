import { pgettext } from "tripetto";
import { IOperation } from "../operation";
import { controls } from "../controls";

export const modOperation: IOperation = {
    name: (operation) => {
        const a = operation.format("value", "", "mod ");
        const b = operation.format("divisor");

        return a && b ? a + b : "";
    },
    title: () => pgettext("block:calculator", "Modulus"),
    editor: (operation, editor) => {
        controls.explanation(
            editor,
            pgettext(
                "block:calculator",
                "Calculates the remainder of a division ([learn more](https://en.wikipedia.org/wiki/Modulo_operation))."
            )
        );

        controls.type(
            operation,
            editor,
            true,
            true,
            true,
            "value",
            false,
            true,
            pgettext("block:calculator", "Input (dividend)")
        );

        controls.type(
            operation,
            editor,
            true,
            true,
            true,
            "divisor",
            false,
            false,
            pgettext("block:calculator", "Divisor")
        );
    },
};
