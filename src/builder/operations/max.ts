import { pgettext } from "tripetto";
import { IOperation } from "../operation";
import { controls } from "../controls";

export const maxOperation: IOperation = {
    name: (operation) => {
        const a = operation.format("value", "*max* (", ",", true);
        const b = operation.format("max", " ", ")");

        return a && b ? a + b : "";
    },
    title: () => pgettext("block:calculator", "Find highest number"),
    editor: (operation, editor) => {
        controls.explanation(
            editor,
            pgettext(
                "block:calculator",
                "Returns the input with the highest number."
            )
        );

        controls.type(
            operation,
            editor,
            true,
            true,
            true,
            "value",
            false,
            true,
            pgettext("block:calculator", "Input 1")
        );

        controls.type(
            operation,
            editor,
            true,
            true,
            true,
            "max",
            false,
            false,
            pgettext("block:calculator", "Input 2")
        );
    },
};
