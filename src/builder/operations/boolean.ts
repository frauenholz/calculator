import {
    Slots,
    isString,
    lookupVariable,
    makeMarkdownSafe,
    pgettext,
} from "tripetto";
import { IOperation } from "../operation";
import { controls } from "../controls";

export const booleanOperation: IOperation = {
    name: (operation) => {
        const value = operation.format(
            "value",
            undefined,
            undefined,
            undefined,
            true
        );
        const outa = operation.format(
            "outa",
            undefined,
            undefined,
            undefined,
            true
        );
        const outb = operation.format(
            "outb",
            undefined,
            undefined,
            undefined,
            true
        );

        if (!value || !outa || !outb) {
            return "";
        }

        const variable =
            (isString(operation.value) &&
                lookupVariable(operation.calculator.block, operation.value)) ||
            undefined;

        return (
            value +
            " = " +
            pgettext(
                "block:calculator",
                "%1 then %2 else %3",
                `**${
                    (variable?.slot instanceof Slots.Boolean &&
                        makeMarkdownSafe(variable.slot.labelForTrue || "")) ||
                    pgettext("block:calculator", "True")
                }**`,
                outa,
                outb
            )
        );
    },
    title: (operation) =>
        (isString(operation.value) &&
            lookupVariable(operation.calculator.block, operation.value)
                ?.label) ||
        pgettext("block:calculator", "Boolean"),
    editor: (operation, editor) => {
        const variable =
            (isString(operation.value) &&
                lookupVariable(operation.calculator.block, operation.value)) ||
            undefined;

        controls.explanation(
            editor,
            pgettext(
                "block:calculator",
                "Outputs a value based on the boolean state."
            )
        );

        controls.type(
            operation,
            editor,
            true,
            true,
            true,
            "outa",
            false,
            false,
            pgettext(
                "block:calculator",
                "If %1 then output",
                `**${
                    (variable?.slot instanceof Slots.Boolean &&
                        makeMarkdownSafe(variable.slot.labelForTrue || "")) ||
                    pgettext("block:calculator", "True")
                }**`
            )
        );

        controls.type(
            operation,
            editor,
            true,
            true,
            true,
            "outb",
            false,
            false,
            pgettext(
                "block:calculator",
                "If %1 then output",
                `**${
                    (variable?.slot instanceof Slots.Boolean &&
                        makeMarkdownSafe(variable.slot.labelForFalse || "")) ||
                    pgettext("block:calculator", "False")
                }**`
            )
        );
    },
};
