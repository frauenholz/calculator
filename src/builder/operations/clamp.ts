import { pgettext } from "tripetto";
import { IOperation } from "../operation";
import { controls } from "../controls";

export const clampOperation: IOperation = {
    name: (operation) => {
        const a = operation.format("value", "*clamp* (", ",", true);
        const b = operation.format("min", " ", ",", true);
        const c = operation.format("max", " ", ")");

        return a && b && c ? a + b + c : "";
    },
    title: () => pgettext("block:calculator", "Clamp number"),
    editor: (operation, editor) => {
        controls.explanation(
            editor,
            pgettext(
                "block:calculator",
                "Clamps (restricts) the input between the specified minimum and maximum value ([learn more](https://en.wikipedia.org/wiki/Clamping_%28graphics%29))."
            )
        );

        controls.type(
            operation,
            editor,
            true,
            true,
            false,
            "value",
            false,
            true,
            pgettext("block:calculator", "Input")
        );

        controls.type(
            operation,
            editor,
            true,
            true,
            true,
            "min",
            false,
            false,
            pgettext("block:calculator", "Minimum value")
        );

        controls.type(
            operation,
            editor,
            true,
            true,
            true,
            "max",
            false,
            false,
            pgettext("block:calculator", "Maximum value")
        );
    },
};
