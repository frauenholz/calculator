import {
    Forms,
    Str,
    insertVariable,
    isFilledString,
    isString,
    makeMarkdownSafe,
    pgettext,
} from "tripetto";
import { IOperation } from "../operation";
import { controls } from "../controls";
import { TModes } from "../../runner/modes";

export const occurrencesOperation: IOperation = {
    name: (operation) => {
        return `${pgettext("block:calculator", "Count")} ${
            operation.compareMode === "regex"
                ? isFilledString(operation.cona) && Str.sanitize(operation.cona)
                    ? `\`${makeMarkdownSafe(operation.cona)}\``
                    : `\`\\_\\_\``
                : isFilledString(operation.cona)
                ? `**${makeMarkdownSafe(
                      Str.replace(
                          Str.replace(operation.cona, "\n", "↵"),
                          " ",
                          "⎵"
                      )
                  )}**`
                : "*\\_\\_*"
        } ${pgettext("block:calculator", "in")} ${operation.format(
            "value",
            undefined,
            undefined,
            undefined,
            true
        )}`;
    },
    title: () => pgettext("block:calculator", "Count occurrences"),
    editor: (operation, editor) => {
        controls.explanation(
            editor,
            pgettext(
                "block:calculator",
                "Counts the number of occurrences of a certain text or character."
            )
        );

        const textGroup = new Forms.Group([
            new Forms.Text(
                "multiline",
                (operation.compareMode !== "regex" &&
                    isString(operation.cona) &&
                    operation.cona) ||
                    ""
            )
                .on((value) => {
                    if (value.isObservable) {
                        operation.cona = value.value || undefined;
                    }
                })
                .sanitize(false)
                .action("@", insertVariable(operation.calculator.block))
                .autoFocus(),
            new Forms.Checkbox(
                pgettext("block:calculator", "Ignore case"),
                Forms.Checkbox.bind(operation, "ignoreCase", undefined, true)
            ),
        ]).visible(operation.compareMode !== "regex");

        const regexGroup = new Forms.Group([
            new Forms.Text(
                "singleline",
                (operation.compareMode === "regex" &&
                    isString(operation.cona) &&
                    operation.cona) ||
                    ""
            )
                .placeholder(
                    pgettext(
                        "block:calculator",
                        "Regex literal (for example /ab+c/)"
                    )
                )
                .on((value) => {
                    if (value.isObservable) {
                        operation.cona = value.value || undefined;
                    }
                })
                .autoValidate((regex) => {
                    if (!regex.value) {
                        return "unknown";
                    }

                    try {
                        const literalSignLeft = regex.value.indexOf("/");
                        const literalSignRight = regex.value.lastIndexOf("/");

                        return literalSignLeft === 0 &&
                            literalSignRight > literalSignLeft &&
                            ((r: string, n: number) => {
                                try {
                                    return (
                                        new RegExp(
                                            r.substring(1, n),
                                            r.substr(n + 1)
                                        ) instanceof RegExp
                                    );
                                } catch {
                                    return false;
                                }
                            })(regex.value, literalSignRight)
                            ? "pass"
                            : "fail";
                    } catch {
                        return "fail";
                    }
                }),
        ]).visible(operation.compareMode === "regex");

        editor.form({
            title: pgettext("block:calculator", "Count"),
            controls: [
                new Forms.Radiobutton<TModes>(
                    [
                        {
                            label: pgettext("block:calculator", "Text"),
                            value: "contains",
                        },
                        {
                            label: pgettext(
                                "block:calculator",
                                "Regular expression"
                            ),
                            value: "regex",
                        },
                    ],
                    operation.compareMode || "contains"
                ).on((mode) => {
                    operation.compareMode = mode.value || "contains";

                    textGroup.visible(operation.compareMode !== "regex");
                    regexGroup.visible(operation.compareMode === "regex");
                }),
                textGroup,
                regexGroup,
            ],
        });
    },
};
