import { pgettext } from "tripetto";
import { IOperation } from "../operation";
import { controls } from "../controls";

export const percentageOperation: IOperation = {
    name: (operation) => {
        const a = operation.format("percentage", "", "% ", true);
        const b = operation.format("value", pgettext("block:calculator", "of"));

        return a && b ? a + b : "";
    },
    title: () => pgettext("block:calculator", "Percentage") + " (%)",
    editor: (operation, editor) => {
        controls.explanation(
            editor,
            pgettext(
                "block:calculator",
                "Calculates a percentage of the input ([learn more](https://en.wikipedia.org/wiki/Percentage))."
            )
        );

        controls.type(
            operation,
            editor,
            true,
            true,
            true,
            "value",
            false,
            true,
            pgettext("block:calculator", "Input")
        );

        controls.type(
            operation,
            editor,
            true,
            false,
            true,
            "percentage",
            false,
            false,
            pgettext("block:calculator", "Percentage")
        );
    },
};
