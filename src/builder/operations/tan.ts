import { pgettext } from "tripetto";
import { IOperation } from "../operation";
import { controls } from "../controls";

export const tanOperation: IOperation = {
    name: (operation) =>
        operation.format(
            "value",
            "*tan* (",
            (operation.angleUnits === "radians"
                ? "rad"
                : operation.angleUnits === "gradians"
                ? "ᵍ"
                : "°") + " )",
            operation.angleUnits !== "radians"
        ),
    title: () => pgettext("block:calculator", "Tangent"),
    editor: (operation, editor) => {
        controls.explanation(
            editor,
            pgettext(
                "block:calculator",
                "Calculates the tangent of the given angle ([learn more](https://en.wikipedia.org/wiki/Trigonometric_functions))."
            )
        );

        const [, numberControl] = controls.type(operation, editor);

        controls.angles(operation, editor, (angles) =>
            numberControl.suffix(
                angles === "radians"
                    ? " rad"
                    : angles === "gradians"
                    ? "ᵍ"
                    : "°"
            )
        );
    },
};
