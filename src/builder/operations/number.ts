import { isString, lookupVariable, pgettext } from "tripetto";
import { IOperation } from "../operation";
import { controls } from "../controls";
import { isConstant } from "../../runner/constants";

export const numberOperation: IOperation = {
    name: (operation) => operation.format("value"),
    title: (operation) =>
        (isString(operation.value)
            ? isConstant(operation.value)
                ? pgettext("block:calculator", "Constant")
                : lookupVariable(operation.calculator.block, operation.value)
                      ?.label
            : "") || pgettext("block:calculator", "Number"),
    editor: (operation, editor) => {
        controls.explanation(
            editor,
            pgettext("block:calculator", "Supplies a number to the calulator.")
        );

        controls.type(
            operation,
            editor,
            (operation.isSubCalculation ||
                !operation.calculator.startBlank ||
                !operation.isFirst) &&
                (operation.operator !== "=" || operation.isFirst),
            true
        );
    },
};
