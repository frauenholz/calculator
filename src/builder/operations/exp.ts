import { pgettext } from "tripetto";
import { IOperation } from "../operation";
import { controls } from "../controls";

export const expOperation: IOperation = {
    name: (operation) => operation.format("exponent", "*exp* (", ")"),
    title: () => pgettext("block:calculator", "Exponent"),
    editor: (operation, editor) => {
        controls.explanation(
            editor,
            pgettext(
                "block:calculator",
                "Calculates `e` to the power of the given exponent ([learn more](https://en.wikipedia.org/wiki/Exponential_function))."
            )
        );

        controls.type(
            operation,
            editor,
            true,
            true,
            true,
            "exponent",
            true,
            true,
            pgettext("block:calculator", "Exponent")
        );
    },
};
