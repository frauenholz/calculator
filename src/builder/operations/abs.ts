import { pgettext } from "tripetto";
import { IOperation } from "../operation";
import { controls } from "../controls";

export const absOperation: IOperation = {
    name: (operation) => operation.format("value", "|", "|"),
    title: () => pgettext("block:calculator", "Absolute value"),
    editor: (operation, editor) => {
        controls.explanation(
            editor,
            pgettext(
                "block:calculator",
                "Retrieves the absolute value (or modulus) of a number ([learn more](https://en.wikipedia.org/wiki/Absolute_value)). This makes negative numbers positive."
            )
        );

        controls.type(operation, editor, true, false, false);
    },
};
