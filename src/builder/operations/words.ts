import { pgettext } from "tripetto";
import { IOperation } from "../operation";
import { controls } from "../controls";

export const wordsOperation: IOperation = {
    name: (operation) =>
        operation.format(
            "value",
            `*${pgettext("block:calculator", "words")}* (`,
            ")"
        ),
    title: () => pgettext("block:calculator", "Word count"),
    editor: (operation, editor) => {
        controls.explanation(
            editor,
            pgettext(
                "block:calculator",
                "Counts the number of words in a text."
            )
        );
    },
};
