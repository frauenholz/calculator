import { pgettext } from "tripetto";
import { IOperation } from "../operation";
import { controls } from "../controls";

export const castOperation: IOperation = {
    name: (operation) =>
        operation.format(
            "value",
            `*${pgettext("block:calculator", "convert")}* (`,
            `) *${pgettext("block:calculator", "to number")}*`
        ),
    title: () => pgettext("block:calculator", "Convert to number"),
    editor: (operation, editor) => {
        controls.explanation(
            editor,
            pgettext("block:calculator", "Converts a text value to a number.")
        );
    },
};
