import { pgettext } from "tripetto";
import { IOperation } from "../operation";
import { controls } from "../controls";

export const sqrtOperation: IOperation = {
    name: (operation) => operation.format("value", "√"),
    title: () => "√",
    editor: (operation, editor) => {
        controls.explanation(
            editor,
            pgettext(
                "block:calculator",
                "Finds the principal square root for the given input ([learn more](https://en.wikipedia.org/wiki/Square_root))."
            )
        );

        controls.type(operation, editor, true, true);
    },
};
