import {
    Forms,
    L10n,
    Slots,
    isNumberFinite,
    isString,
    lookupVariable,
    pgettext,
} from "tripetto";
import { IOperation, Operation } from "../operation";
import { controls } from "../controls";
import { TModes } from "../../runner/modes";

function format(operation: Operation, property: "cona" | "conb"): string {
    const input = operation[property];
    const variable =
        isString(operation.value) &&
        lookupVariable(operation.calculator.block, operation.value);
    const slot =
        (variable && variable.slot instanceof Slots.Date && variable.slot) ||
        undefined;

    if (isString(input)) {
        if (input && lookupVariable(operation.calculator.block, input)?.label) {
            return `@${input}`;
        }

        return "";
    } else if (isNumberFinite(input)) {
        return `**${
            (slot ? slot.supportsTime : operation.opcode === "datetime")
                ? L10n.locale.dateTimeShort(input, true)
                : L10n.locale.dateShort(input, true)
        }**`;
    } else {
        return "`" + pgettext("block:calculator", "Now").toUpperCase() + "`";
    }
}

export const dateOperation: IOperation = {
    name: (operation) => {
        const value = operation.format(
            "value",
            undefined,
            undefined,
            undefined,
            true,
            "NOW"
        );
        const cona = format(operation, "cona");
        const conb = format(operation, "conb");
        const outa = operation.format(
            "outa",
            undefined,
            undefined,
            undefined,
            true
        );
        const outb = operation.format(
            "outb",
            undefined,
            undefined,
            undefined,
            true
        );

        if (!value || !outa || !outb) {
            return "";
        }

        const out = `then ${outa} else ${outb}`;

        switch (operation.compareMode || "equal") {
            case "defined":
                return `${value} ${pgettext(
                    "block:calculator",
                    "not empty"
                )} ${out}`;
            case "between":
                return (
                    (cona && conb && `${cona} ≤ ${value} ≤ ${conb} ${out}`) ||
                    ""
                );
            case "before":
            case "after":
            case "equal":
                return (
                    (cona &&
                        `${value} ${
                            operation.compareMode === "after"
                                ? ">"
                                : operation.compareMode === "before"
                                ? "<"
                                : "="
                        } ${cona} ${out}`) ||
                    ""
                );
        }
    },
    title: (operation) =>
        (isString(operation.value)
            ? lookupVariable(operation.calculator.block, operation.value)?.label
            : "") ||
        (operation.opcode === "datetime"
            ? pgettext("block:calculator", "Current date + time")
            : pgettext("block:calculator", "Current date")),
    editor: (operation, editor) => {
        const variable =
            isString(operation.value) &&
            lookupVariable(operation.calculator.block, operation.value);
        const slot =
            (variable &&
                variable.slot instanceof Slots.Date &&
                variable.slot) ||
            undefined;
        const supportsTime = slot
            ? slot.supportsTime
            : operation.opcode === "datetime";
        const compareFunction = new Forms.Radiobutton<TModes>(
            [
                {
                    label: pgettext("block:calculator", "Date is equal to"),
                    value: "equal",
                },
                {
                    label: pgettext("block:calculator", "Date is before"),
                    value: "before",
                },
                {
                    label: pgettext("block:calculator", "Date is after"),
                    value: "after",
                },
                {
                    label: pgettext("block:calculator", "Date is between"),
                    value: "between",
                },
                {
                    label: pgettext("block:calculator", "Date is not empty"),
                    value: "defined",
                    visible: slot ? true : false,
                },
            ],
            operation.compareMode || "equal"
        ).on((mode) => {
            operation.compareMode = mode.value || "equal";

            conditionA.visible(operation.compareMode !== "defined");
            conditionB.visible(mode.value === "between");

            switch (operation.compareMode) {
                case "equal":
                    conditionA.title = pgettext(
                        "block:calculator",
                        "If date equals"
                    );
                    break;
                case "before":
                    conditionA.title = pgettext(
                        "block:calculator",
                        "If date is before"
                    );
                    break;
                case "after":
                    conditionA.title = pgettext(
                        "block:calculator",
                        "If date is after"
                    );
                    break;
                case "between":
                    conditionA.title = pgettext(
                        "block:calculator",
                        "If date is between"
                    );
                    break;
            }
        });

        controls.explanation(
            editor,
            pgettext(
                "block:calculator",
                "Compares a date and outputs a value based on the result of the comparison."
            )
        );

        editor.form({
            title: pgettext("block:calculator", "Compare mode"),
            controls: [compareFunction],
        });

        const addCondition = (
            property: "cona" | "conb",
            title: string,
            visible: boolean
        ) => {
            const value = operation[property];
            const dateControl = new Forms.DateTime(
                isNumberFinite(value) ? value : undefined
            )
                .zone("UTC")
                .years(
                    new Date().getFullYear() - 150,
                    new Date().getFullYear() + 50
                )
                .label(
                    supportsTime
                        ? pgettext("block:calculator", "Use fixed date/time")
                        : pgettext("block:calculator", "Use fixed date")
                )
                .features(
                    Forms.DateTimeFeatures.Date |
                        (supportsTime
                            ? Forms.DateTimeFeatures.TimeHoursAndMinutesOnly
                            : Forms.DateTimeFeatures.Weekday)
                )
                .required()
                .on((input) => {
                    if (input.isFormVisible && input.isObservable) {
                        operation[property] = input.value;
                    }
                });

            const variableControl = controls.variable(
                operation,
                editor,
                property,
                "inline",
                false,
                (s) => s instanceof Slots.Date && s.id !== operation.value
            );

            return editor
                .form({
                    title,
                    controls: [
                        new Forms.Radiobutton<"current" | "variable" | "date">(
                            [
                                {
                                    label: pgettext(
                                        "block:calculator",
                                        "Current date"
                                    ),
                                    value: "current",
                                    visible: variable ? true : false,
                                },
                                {
                                    label: supportsTime
                                        ? pgettext(
                                              "block:calculator",
                                              "Fixed date/time"
                                          )
                                        : pgettext(
                                              "block:calculator",
                                              "Fixed date"
                                          ),
                                    value: "date",
                                },
                                {
                                    label: pgettext(
                                        "block:calculator",
                                        "Value"
                                    ),
                                    value: "variable",
                                    disabled: variableControl[1] === 0,
                                },
                            ],
                            isString(value)
                                ? "variable"
                                : isNumberFinite(value)
                                ? "date"
                                : "current"
                        ).on((type) => {
                            dateControl.visible(type.value === "date");
                            variableControl[0].visible(
                                type.value === "variable"
                            );

                            if (type.value === "current") {
                                operation[property] = undefined;
                            }
                        }),
                        dateControl,
                        variableControl[0] as Forms.Dropdown<string>,
                    ],
                })
                .visible(visible);
        };

        const conditionA = addCondition(
            "cona",
            pgettext("block:calculator", "If date equals"),
            operation.compareMode !== "defined"
        );
        const conditionB = addCondition(
            "conb",
            pgettext("block:calculator", "And"),
            operation.compareMode === "between"
        );

        controls.type(
            operation,
            editor,
            true,
            true,
            true,
            "outa",
            false,
            false,
            pgettext("block:calculator", "Then output")
        );

        controls.type(
            operation,
            editor,
            true,
            true,
            true,
            "outb",
            false,
            false,
            pgettext("block:calculator", "Else output")
        );
    },
};
