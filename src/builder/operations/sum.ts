import { isString, lookupVariable, pgettext } from "tripetto";
import { IOperation } from "../operation";
import { controls } from "../controls";

export const sumOperation: IOperation = {
    name: (operation) => operation.format("value", "\u2211"),
    title: (operation) =>
        (isString(operation.value)
            ? lookupVariable(operation.calculator.block, operation.value)?.label
            : "") || pgettext("block:calculator", "Sum"),
    editor: (operation, editor) => {
        controls.explanation(
            editor,
            pgettext(
                "block:calculator",
                "Adds up the values of all given answers."
            )
        );
    },
};
