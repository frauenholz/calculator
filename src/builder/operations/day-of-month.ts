import { isString, lookupVariable, pgettext } from "tripetto";
import { IOperation } from "../operation";
import { controls } from "../controls";

export const dayOfMonthOperation: IOperation = {
    name: (operation) =>
        operation.format(
            "value",
            `*${pgettext("block:calculator", "day of month")}* (`,
            ")"
        ),
    title: (operation) =>
        (isString(operation.value)
            ? lookupVariable(operation.calculator.block, operation.value)?.label
            : "") || pgettext("block:calculator", "Day of month"),
    editor: (operation, editor) => {
        controls.explanation(
            editor,
            pgettext(
                "block:calculator",
                "Retrieves the day of month of the date."
            )
        );
    },
};
