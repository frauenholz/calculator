import { pgettext } from "tripetto";
import { IOperation } from "../operation";
import { controls } from "../controls";

export const sgnOperation: IOperation = {
    name: (operation) => operation.format("value", `*sgn* (`, ")"),
    title: () => pgettext("block:calculator", "Sign"),
    editor: (operation, editor) => {
        controls.explanation(
            editor,
            pgettext(
                "block:calculator",
                "Extracts the sign of a number ([learn more](https://en.wikipedia.org/wiki/Sign_function)). For positive numbers this function returns `1` and for negative numbers `-1`. When the input is zero the function returns `0`."
            )
        );

        controls.type(operation, editor);
    },
};
