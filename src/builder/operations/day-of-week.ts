import { isString, lookupVariable, pgettext } from "tripetto";
import { IOperation } from "../operation";
import { controls } from "../controls";

export const dayOfWeekOperation: IOperation = {
    name: (operation) =>
        operation.format(
            "value",
            `*${pgettext("block:calculator", "day of week")}* (`,
            ")"
        ),
    title: (operation) =>
        (isString(operation.value)
            ? lookupVariable(operation.calculator.block, operation.value)?.label
            : "") || pgettext("block:calculator", "Day of week"),
    editor: (operation, editor) => {
        controls.explanation(
            editor,
            pgettext(
                "block:calculator",
                "Retrieves the day of the week (where 0 represents Sunday) of the date."
            )
        );
    },
};
