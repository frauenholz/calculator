import { pgettext } from "tripetto";
import { IOperation } from "../operation";
import { controls } from "../controls";

export const minOperation: IOperation = {
    name: (operation) => {
        const a = operation.format("value", "*min* (", ",", true);
        const b = operation.format("min", " ", ")");

        return a && b ? a + b : "";
    },
    title: () => pgettext("block:calculator", "Find lowest number"),
    editor: (operation, editor) => {
        controls.explanation(
            editor,
            pgettext(
                "block:calculator",
                "Returns the input with the lowest number."
            )
        );

        controls.type(
            operation,
            editor,
            true,
            true,
            true,
            "value",
            false,
            true,
            pgettext("block:calculator", "Input 1")
        );

        controls.type(
            operation,
            editor,
            true,
            true,
            true,
            "min",
            false,
            false,
            pgettext("block:calculator", "Input 2")
        );
    },
};
