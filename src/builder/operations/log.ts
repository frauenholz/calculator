import { pgettext } from "tripetto";
import { IOperation } from "../operation";
import { controls } from "../controls";

export const logOperation: IOperation = {
    name: (operation) => operation.format("value", "*log* (", ")"),
    title: () => pgettext("block:calculator", "Logarithm"),
    editor: (operation, editor) => {
        controls.explanation(
            editor,
            pgettext(
                "block:calculator",
                "Calculates the base 10 logarithm of the input ([learn more](https://en.wikipedia.org/wiki/Logarithm))."
            )
        );

        controls.type(operation, editor, true, true);
    },
};
