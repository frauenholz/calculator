import { isString, lookupVariable, pgettext } from "tripetto";
import { IOperation } from "../operation";
import { controls } from "../controls";

export const minuteOperation: IOperation = {
    name: (operation) =>
        operation.format(
            "value",
            `*${pgettext("block:calculator", "minute")}* (`,
            ")"
        ),
    title: (operation) =>
        (isString(operation.value)
            ? lookupVariable(operation.calculator.block, operation.value)?.label
            : "") || pgettext("block:calculator", "Minute"),
    editor: (operation, editor) => {
        controls.explanation(
            editor,
            pgettext(
                "block:calculator",
                "Retrieves a number between 0 and 59, representing the minutes of the date/time."
            )
        );
    },
};
