import { pgettext } from "tripetto";
import { IOperation } from "../operation";
import { controls } from "../controls";

export const lnOperation: IOperation = {
    name: (operation) => operation.format("value", "*ln* (", ")"),
    title: () => pgettext("block:calculator", "Natural logarithm"),
    editor: (operation, editor) => {
        controls.explanation(
            editor,
            pgettext(
                "block:calculator",
                "Calculates the natural logarithm of the input ([learn more](https://en.wikipedia.org/wiki/Natural_logarithm))."
            )
        );

        controls.type(operation, editor, true, true);
    },
};
