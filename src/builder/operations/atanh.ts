import { pgettext } from "tripetto";
import { IOperation } from "../operation";
import { controls } from "../controls";

export const atanhOperation: IOperation = {
    name: (operation) => operation.format("value", "*tanh⁻¹* (", ")"),
    title: () => pgettext("block:calculator", "Inverse hyperbolic tangent"),
    editor: (operation, editor) => {
        controls.explanation(
            editor,
            pgettext(
                "block:calculator",
                "Calculates the inverse hyperbolic tangent (arctangent) of the given number ([learn more](https://en.wikipedia.org/wiki/Inverse_hyperbolic_functions))."
            )
        );

        controls.type(operation, editor);
    },
};
