import { isString, lookupVariable, pgettext } from "tripetto";
import { IOperation } from "../operation";
import { controls } from "../controls";

export const countOperation: IOperation = {
    name: (operation) =>
        operation.format(
            "value",
            `*${pgettext("block:calculator", "count")}* (`,
            ")"
        ),
    title: (operation) =>
        (isString(operation.value)
            ? lookupVariable(operation.calculator.block, operation.value)?.label
            : "") || pgettext("block:calculator", "Count"),
    editor: (operation, editor) => {
        controls.explanation(
            editor,
            pgettext(
                "block:calculator",
                "Counts the number of selected options or given answers. Useful if you want to calculate the average of given answers."
            )
        );
    },
};
