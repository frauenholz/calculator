import { pgettext } from "tripetto";
import { IOperation } from "../operation";
import { controls } from "../controls";

export const roundOperation: IOperation = {
    name: (operation) => operation.format("value", `*round* (`, ")"),
    title: () => pgettext("block:calculator", "Round"),
    editor: (operation, editor) => {
        controls.explanation(
            editor,
            pgettext(
                "block:calculator",
                "Rounds a floating point number ([learn more](https://en.wikipedia.org/wiki/Rounding))."
            )
        );

        controls.type(operation, editor, true, true);
    },
};
