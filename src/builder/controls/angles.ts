import { EditorOrchestrator, Forms, pgettext, setAny } from "tripetto";
import { Operation } from "../operation";

export const angleUnitsControl = (
    operation: Operation,
    editor: EditorOrchestrator<Operation>,
    onChange?: (angles: "degrees" | "radians" | "gradians") => void,
    property: keyof typeof operation = "angleUnits"
) => {
    const value = operation[property];

    return editor.form({
        title: pgettext("block:calculator", "Units"),
        controls: [
            new Forms.Radiobutton<"degrees" | "radians" | "gradians">(
                [
                    {
                        label: pgettext("block:calculator", "Degrees") + " (°)",
                        value: "degrees",
                    },
                    {
                        label:
                            pgettext("block:calculator", "Radians") + " (rad)",
                        value: "radians",
                    },
                    {
                        label:
                            pgettext("block:calculator", "Gradians") + " (ᵍ)",
                        value: "gradians",
                    },
                ],
                value === "radians" || value === "gradians" ? value : "degrees"
            ).on((units) => {
                setAny(operation, property, units.value || "degrees");

                if (onChange) {
                    onChange(units.value || "degrees");
                }
            }),
        ],
    });
};
