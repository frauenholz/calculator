import {
    EditorOrchestrator,
    Forms,
    L10n,
    castToString,
    pgettext,
    setAny,
} from "tripetto";
import { Operation } from "../operation";
import { CONSTANTS, TConstants, castToConstant } from "../../runner/constants";

export function getConstantLabel(constant: TConstants): string {
    switch (constant) {
        case "π":
            return `π ≈ ${L10n.locale.number(3.14159, "auto")}`;
        case "e":
            return `e ≈ ${L10n.locale.number(2.71828, "auto")}`;
        case "γ":
            return `γ ≈ ${L10n.locale.number(0.57722, "auto")}`;
        case "c":
            return `c = ${L10n.locale.number(299792458)} m/s`;
        case "random":
            return pgettext(
                "block:calculator",
                "Random value (0 to less than 1)"
            );
        case "timestamp":
            return pgettext(
                "block:calculator",
                "UNIX time (seconds since Unix Epoch)"
            );
        case "year":
            return pgettext("block:calculator", "Year");
        case "month":
            return pgettext("block:calculator", "Month (January = 1)");
        case "day":
            return pgettext("block:calculator", "Day of month (1-31)");
        case "day-of-week":
            return pgettext("block:calculator", "Day of week (Sunday = 0)");
        case "hour":
            return pgettext("block:calculator", "Hour (0-23)");
        case "minute":
            return pgettext("block:calculator", "Minute (0-59)");
        case "second":
            return pgettext("block:calculator", "Second (0-59)");
        case "millisecond":
            return pgettext("block:calculator", "Millisecond (0-999)");
        case "timezone":
            return pgettext("block:calculator", "Timezone (in milliseconds)");
        case "branch":
            return pgettext("block:calculator", "Branch number");
    }
}

export function getConstantIcon(constant: TConstants): number {
    switch (constant) {
        case "π":
        case "e":
        case "γ":
        case "c":
            return 0xe6da;
        case "random":
            return 0xe687;
        case "timestamp":
            return 0xe78a;
        case "year":
        case "month":
        case "day-of-week":
            return 0xe789;
        case "day":
            return 0xe788;
        case "hour":
        case "minute":
        case "second":
        case "millisecond":
            return 0xe8e8;
        case "timezone":
            return 0xe886;
        case "branch":
            return 0xe947;
    }
}

export const constantControl = (
    operation: Operation,
    editor: EditorOrchestrator<Operation>,
    property: keyof typeof operation = "value",
    mode: "form" | "inline" = "form",
    autoFocus: boolean = true
) => {
    const control = new Forms.Dropdown<TConstants>(
        CONSTANTS.map((constant) => ({
            label: getConstantLabel(constant),
            value: constant,
        })),
        castToString(operation[property]) as TConstants
    )
        .autoFocus(autoFocus)
        .on((constant) => {
            if (constant.isFormVisible && constant.isObservable) {
                setAny(operation, property, castToConstant(constant.value));
            }
        });

    if (mode === "inline") {
        return control.label(pgettext("block:calculator", "Use constant"));
    }

    return editor.form({
        title: pgettext("block:calculator", "Use constant"),
        controls: [control],
    });
};
