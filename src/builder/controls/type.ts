import {
    EditorOrchestrator,
    Forms,
    IVariable,
    castToString,
    isNumberFinite,
    isString,
    lookupVariable,
    pgettext,
    setAny,
} from "tripetto";
import { Operation } from "../operation";
import { constantControl } from "./constant";
import { numberControl } from "./number";
import { variableControl } from "./variable";
import { isConstant } from "../../runner/constants";

export const typeControl = (
    operation: Operation,
    editor: EditorOrchestrator<Operation>,
    showANS: boolean = true,
    showConstants: boolean = false,
    showNumber: boolean = true,
    property: keyof typeof operation = "value",
    autoClose: boolean = true,
    autoFocus: boolean = true,
    title?: string,
    onChange?: (
        type: "number" | "ans" | "variable" | "constant",
        variable?: IVariable
    ) => void
): [Forms.Form, Forms.Numeric] => {
    const value = operation[property];
    const mode = title ? "inline" : "form";
    let form: Forms.Form;
    const control = new Forms.Radiobutton<
        "number" | "ans" | "variable" | "constant"
    >(
        [
            ...(showANS
                ? [
                      {
                          label:
                              operation.isSubCalculation && operation.isFirst
                                  ? pgettext(
                                        "block:calculator",
                                        "Last outcome *(%1 parent calculation)*",
                                        "ANS"
                                    )
                                  : operation.isFirst &&
                                    !operation.calculator.startBlank
                                  ? pgettext(
                                        "block:calculator",
                                        "Initial value"
                                    )
                                  : pgettext(
                                        "block:calculator",
                                        "Last outcome *(%1)*",
                                        "ANS"
                                    ),
                          value: "ans" as "ans",
                          disabled: !operation.allowANS,
                          markdown: true,
                      },
                  ]
                : []),
            ...(showNumber
                ? [
                      {
                          label: pgettext("block:calculator", "Number"),
                          value: "number" as "number",
                      },
                  ]
                : []),
            {
                label: pgettext("block:calculator", "Value"),
                value: "variable",
            },
            ...(showConstants
                ? [
                      {
                          label: pgettext("block:calculator", "Constant"),
                          value: "constant" as "constant",
                      },
                  ]
                : []),
        ],

        isString(value)
            ? isConstant(value)
                ? "constant"
                : "variable"
            : isNumberFinite(value) || !showANS
            ? "number"
            : "ans"
    ).on((type) => {
        numberForm[0].visible(type.value === "number");
        variableForm[0].visible(type.value === "variable");
        constantForm.visible(type.value === "constant");

        if (type.value === "ans") {
            setAny(operation, property, undefined);
        }

        if (onChange) {
            onChange(
                type.value || "number",
                (type.value === "variable" &&
                    variableForm[2].value &&
                    lookupVariable(
                        operation.calculator.block,
                        variableForm[2].value
                    )) ||
                    undefined
            );
        }
    });

    if (mode === "form") {
        form = editor.form({
            title: pgettext("block:calculator", "Input"),
            controls: [control],
        });
    }

    const numberForm = numberControl(
        operation,
        editor,
        property,
        autoClose,
        autoFocus,
        mode
    );
    const variableForm = variableControl(
        operation,
        editor,
        property,
        mode,
        autoFocus,
        undefined,
        onChange &&
            ((variable) => {
                onChange!(
                    control.value || "number",
                    (control.value === "variable" &&
                        variable &&
                        lookupVariable(operation.calculator.block, variable)) ||
                        undefined
                );
            })
    );
    const constantForm = constantControl(
        operation,
        editor,
        property,
        mode,
        autoFocus
    ).visible(isConstant(castToString(value)));

    numberForm[0].visible(
        isNumberFinite(value) || (!showANS && !isString(value))
    );

    variableForm[0].visible(isString(value));

    control.buttonDisabled("variable", variableForm[1] === 0);

    if (
        numberForm[0] instanceof Forms.Control &&
        variableForm[0] instanceof Forms.Control &&
        constantForm instanceof Forms.Control
    ) {
        form = editor.form({
            title: title,
            markdown: true,
            controls: [control, numberForm[0], variableForm[0], constantForm],
        });
    }

    return [form!, numberForm[1]];
};
