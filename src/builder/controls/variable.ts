import {
    EditorOrchestrator,
    Forms,
    Slots,
    isString,
    pgettext,
    populateVariables,
    setAny,
} from "tripetto";
import { Operation } from "../operation";

export const variableControl = (
    operation: Operation,
    editor: EditorOrchestrator<Operation>,
    property: keyof typeof operation = "value",
    mode: "form" | "inline" = "form",
    autoFocus: boolean = true,
    filter: (slot: Slots.Slot) => boolean = (slot) =>
        slot instanceof Slots.Number || slot instanceof Slots.Numeric,
    onChange?: (value: string) => void
): [Forms.Form | Forms.Dropdown<string>, number, Forms.Dropdown<string>] => {
    const value = operation[property];
    const variables = populateVariables(
        operation.calculator.block,
        filter,
        isString(value) ? value : undefined
    );
    const control = new Forms.Dropdown(variables, isString(value) ? value : "")
        .placeholder(
            variables.length === 0
                ? pgettext("block:calculator", "No usable values found...")
                : ""
        )
        .autoFocus(autoFocus)
        .on((slot) => {
            if (slot.isFormVisible && slot.isObservable) {
                setAny(operation, property, slot.value || "");
            }

            if (onChange) {
                onChange(slot.value || "");
            }
        });

    return [
        mode === "form"
            ? editor.form({
                  title: pgettext("block:calculator", "Use value of"),
                  controls: [control],
              })
            : control.label(pgettext("block:calculator", "Use value of")),
        variables.length,
        control,
    ];
};
