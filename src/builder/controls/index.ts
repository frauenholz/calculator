import { angleUnitsControl } from "./angles";
import { explanationControl } from "./explanation";
import { numberControl } from "./number";
import { typeControl } from "./type";
import { variableControl } from "./variable";
import { constantControl } from "./constant";

export const controls = {
    angles: angleUnitsControl,
    constant: constantControl,
    explanation: explanationControl,
    number: numberControl,
    type: typeControl,
    variable: variableControl,
};
