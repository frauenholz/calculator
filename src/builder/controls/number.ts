import {
    EditorOrchestrator,
    Forms,
    isNumberFinite,
    pgettext,
    setAny,
} from "tripetto";
import { Operation } from "../operation";

export const numberControl = (
    operation: Operation,
    editor: EditorOrchestrator<Operation>,
    property: keyof typeof operation = "value",
    autoClose: boolean = true,
    autoFocus: boolean = true,
    mode: "form" | "inline" = "form"
): [Forms.Form | Forms.Numeric, Forms.Numeric] => {
    const value = operation[property];
    const control = new Forms.Numeric(isNumberFinite(value) ? value : 0)
        .precision("auto")
        .thousands(false)
        .autoFocus(autoFocus)
        .on((input) => {
            if (input.isFormVisible && input.isObservable) {
                setAny(operation, property, input.value);
            }
        });

    if (autoClose) {
        control.enter(editor.close);
        control.escape(editor.close);
    }

    if (property === "percentage") {
        control.suffix("%");
        control.min(0);
    }

    return [
        mode === "form"
            ? editor.form({
                  title: pgettext("block:calculator", "Use fixed number"),
                  controls: [control],
              })
            : control.label(pgettext("block:calculator", "Use fixed number")),
        control,
    ];
};
