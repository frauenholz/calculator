/** Dependencies */
import {
    Cluster,
    Collection,
    Components,
    DateTime,
    ISlotIdentifier,
    Slots,
    each,
    findFirst,
    getMetadata,
    getSlotIcon,
    pgettext,
    populateSlots,
} from "tripetto";
import { Calculator } from "./calculator";
import { ICalculator } from "./interface";
import { Operation } from "./operation";
import { TOperators } from "../runner/operators";
import { CONSTANTS } from "../runner/constants";
import { getConstantIcon, getConstantLabel } from "./controls/constant";
import { ICalculatorMetadata } from "./metadata";

/** Assets */
import ICON_EQUAL from "../../assets/equal.svg";
import ICON_ADD from "../../assets/add.svg";
import ICON_SUBTRACT from "../../assets/subtract.svg";
import ICON_MULTIPLY from "../../assets/multiply.svg";
import ICON_DIVIDE from "../../assets/divide.svg";
import ICON_EXPONENTIATION from "../../assets/exponentiation.svg";
import ICON_NUMBER from "../../assets/number.svg";
import ICON_COUNTER from "../../assets/counter.svg";

export const operatorMenu = (
    calculator: ICalculator,
    isSubCalculation: boolean = false
) =>
    calculator.startBlank &&
    calculator.operations.count === 0 &&
    !isSubCalculation
        ? operationsMenu(calculator, false)("+")
        : [
              new Components.MenuSubmenuWithImage(
                  ICON_ADD,
                  pgettext("block:calculator", "Add"),
                  () => operationsMenu(calculator, isSubCalculation)("+")
              ),
              new Components.MenuSubmenuWithImage(
                  ICON_SUBTRACT,
                  pgettext("block:calculator", "Subtract"),
                  () => operationsMenu(calculator, isSubCalculation)("-")
              ),
              new Components.MenuSubmenuWithImage(
                  ICON_MULTIPLY,
                  pgettext("block:calculator", "Multiply"),
                  () => operationsMenu(calculator, isSubCalculation)("*")
              ),
              new Components.MenuSubmenuWithImage(
                  ICON_DIVIDE,
                  pgettext("block:calculator", "Divide"),
                  () => operationsMenu(calculator, isSubCalculation)("/")
              ),
              new Components.MenuSeparator(),
              new Components.MenuSubmenuWithImage(
                  ICON_EQUAL,
                  pgettext("block:calculator", "Equal"),
                  () => operationsMenu(calculator, isSubCalculation)("=")
              ),
          ];

export const operatorChangeMenu = (operation: Operation) => [
    new Components.MenuItemWithImage(
        ICON_ADD,
        pgettext("block:calculator", "Add"),
        () => (operation.operator = "+"),
        operation.operator === "+"
    ),
    new Components.MenuItemWithImage(
        ICON_SUBTRACT,
        pgettext("block:calculator", "Subtract"),
        () => (operation.operator = "-"),
        operation.operator === "-"
    ),
    new Components.MenuItemWithImage(
        ICON_MULTIPLY,
        pgettext("block:calculator", "Multiply"),
        () => (operation.operator = "*"),
        operation.operator === "*"
    ),
    new Components.MenuItemWithImage(
        ICON_DIVIDE,
        pgettext("block:calculator", "Divide"),
        () => (operation.operator = "/"),
        operation.operator === "/"
    ),
    new Components.MenuSeparator(),
    new Components.MenuItemWithImage(
        ICON_EQUAL,
        pgettext("block:calculator", "Equal"),
        () => (operation.operator = "="),
        operation.operator === "="
    ),
];

const operationsMenu =
    (calculator: ICalculator, isSubCalculation: boolean) =>
    (operator: TOperators) => {
        const isNumber =
            isSubCalculation ||
            (calculator.startBlank && calculator.operations.count === 0) ||
            operator !== "=";
        const menu: Components.MenuOption[] = [];

        menu.push(
            new Components.MenuItemWithIcon(
                0xe776,
                pgettext("block:calculator", "Number"),
                () =>
                    calculator.operations.append().set({
                        operator,
                        opcode: "number",
                        value: operator === "/" ? 1 : 0,
                        open: true,
                    })
            )
        );

        const slots = blocksMenu(calculator, operator);

        menu.push(
            new Components.MenuSubmenuWithIcon(
                0xe6e1,
                pgettext("block:calculator", "Score"),
                scoreMenu(calculator, operator),
                false,
                false
            ),
            new Components.MenuSubmenuWithIcon(
                0xe839,
                pgettext("block:calculator", "Comparison"),
                () => {
                    const compare = compareMenu(calculator, operator);

                    return [
                        new Components.MenuItemWithIcon(
                            calculator.startBlank ||
                            calculator.operations.count > 0
                                ? 0xe767
                                : 0xe8fc,
                            calculator.startBlank ||
                            calculator.operations.count > 0
                                ? pgettext(
                                      "block:calculator",
                                      "Last outcome (%1)",
                                      "ANS"
                                  )
                                : pgettext("block:calculator", "Initial value"),
                            () =>
                                calculator.operations.append().set({
                                    operator,
                                    opcode: "equation",
                                    value: undefined,
                                    cona: 0,
                                    conb: 0,
                                    outa: 1,
                                    outb: 0,
                                    open: true,
                                }),
                            calculator.operations.count === 0 &&
                                calculator.startBlank
                        ),
                        new Components.MenuSubmenuWithIcon(
                            0xe789,
                            pgettext("block:calculator", "Date/time"),
                            [
                                new Components.MenuItem(
                                    pgettext(
                                        "block:calculator",
                                        "Current date"
                                    ),
                                    () =>
                                        calculator.operations.append().set({
                                            operator,
                                            opcode: "date",
                                            value: undefined,
                                            cona: DateTime.UTCToday,
                                            conb: undefined,
                                            outa: 1,
                                            outb: 0,
                                            open: true,
                                        })
                                ),
                                new Components.MenuItem(
                                    pgettext(
                                        "block:calculator",
                                        "Current date + time"
                                    ),
                                    () =>
                                        calculator.operations.append().set({
                                            operator,
                                            opcode: "datetime",
                                            value: undefined,
                                            cona: DateTime.UTC,
                                            conb: undefined,
                                            outa: 1,
                                            outb: 0,
                                            open: true,
                                        })
                                ),
                            ]
                        ),
                        ...compare,
                    ];
                }
            ),
            new Components.MenuSubmenuWithIcon(
                0xe62f,
                pgettext("block:calculator", "Function"),
                () => functionsMenu(operator, calculator.operations, isNumber)
            ),
            new Components.MenuItemWithIcon(
                0xe766,
                pgettext("block:calculator", "Subcalculation"),
                () =>
                    calculator.operations.append().set({
                        operator,
                        opcode: "calc",
                        open: true,
                    })
            )
        );

        if (slots.length > 0) {
            if (!(slots[0] instanceof Components.MenuSeparator)) {
                menu.push(
                    new Components.MenuSeparator(),
                    new Components.MenuLabel(
                        pgettext("block:calculator", "Blocks")
                    )
                );
            }

            menu.push(...slots);
        }

        return menu;
    };

const scoreMenu = (calculator: ICalculator, operator: TOperators) => {
    const menu: Components.MenuOption[] = [];
    let cluster: string | undefined;

    const addCluster = (src: Cluster | undefined) => {
        if (src?.id !== cluster) {
            const name = src?.name;

            if (menu.length > 0) {
                menu.push(new Components.MenuSeparator());
            }

            if (name) {
                menu.push(new Components.MenuLabel(name));
            }

            cluster = src?.id;
        }
    };

    const addScore = (parent: ISlotIdentifier, identifier: ISlotIdentifier) => {
        const scoreSupply = Collection.find(identifier);
        const metadata = getMetadata<ICalculatorMetadata>(
            identifier.block,
            "calculator"
        );

        if (
            scoreSupply ||
            (metadata &&
                (metadata[identifier.slot?.reference || "*"] || metadata["*"])
                    ?.scores)
        ) {
            addCluster(parent.block.node.cluster);

            const scoreSlot = identifier.slot?.slots?.select(
                "score",
                "feature"
            );

            if (scoreSlot) {
                const children: Components.MenuOption[] = [];

                addSlot(
                    children,
                    operator,
                    calculator.operations,
                    {
                        type: "slot",
                        block: parent.block,
                        label:
                            scoreSlot.alias ||
                            pgettext("block:calculator", "Existing score"),
                        icon: parent.icon || parent.block.type.icon,
                        id: scoreSlot.id,
                        reference: scoreSlot.reference,
                        slot: scoreSlot,
                    },
                    false,
                    true
                );

                children.push(
                    new Components.MenuItemWithIcon(
                        0xe70b,
                        pgettext("block:calculator", "New score"),
                        () =>
                            calculator.operations.append().set({
                                operator,
                                opcode: "score",
                                value: identifier.id,
                                open: true,
                            })
                    )
                );

                menu.push(
                    new Components.MenuSubmenuWithImage(
                        parent.icon || parent.block.type.icon,
                        parent.label,
                        children
                    )
                );
            } else {
                menu.push(
                    new Components.MenuItemWithImage(
                        parent.icon || parent.block.type.icon,
                        parent.label,
                        () =>
                            calculator.operations.append().set({
                                operator,
                                opcode: "score",
                                value: identifier.id,
                                open: true,
                            })
                    )
                );
            }
        }
    };

    each(
        populateSlots(calculator.block, {
            mode: "validated",
            pipes: "include-consumed-slots",
        }),
        (identifier) => {
            if (identifier.slots) {
                findFirst(identifier.slots, (pipe) => {
                    if (pipe.type === "pipe") {
                        const supply = Collection.find(pipe);

                        if (supply) {
                            const scoreSlot = pipe.slot?.slots?.select(
                                "score",
                                "feature"
                            );

                            addCluster(identifier.block.node.cluster);

                            if (scoreSlot) {
                                const children: Components.MenuOption[] = [];

                                addSlot(
                                    children,
                                    operator,
                                    calculator.operations,
                                    {
                                        type: "slot",
                                        block: identifier.block,
                                        label:
                                            scoreSlot.alias ||
                                            pgettext(
                                                "block:calculator",
                                                "Existing score"
                                            ),
                                        icon:
                                            identifier.icon ||
                                            identifier.block.type.icon,
                                        id: scoreSlot.id,
                                        reference: scoreSlot.reference,
                                        slot: scoreSlot,
                                    },
                                    false,
                                    true
                                );

                                children.push(
                                    new Components.MenuItemWithIcon(
                                        0xe70b,
                                        pgettext(
                                            "block:calculator",
                                            "New score"
                                        ),
                                        () =>
                                            calculator.operations.append().set({
                                                operator,
                                                opcode: "score",
                                                value: pipe.id,
                                                open: true,
                                            })
                                    )
                                );

                                menu.push(
                                    new Components.MenuSubmenuWithImage(
                                        identifier.icon ||
                                            identifier.block.type.icon,
                                        identifier.label,
                                        children
                                    )
                                );
                            } else {
                                menu.push(
                                    new Components.MenuItemWithImage(
                                        identifier.icon ||
                                            identifier.block.type.icon,
                                        identifier.label,
                                        () =>
                                            calculator.operations.append().set({
                                                operator,
                                                opcode: "score",
                                                value: pipe.id,
                                                open: true,
                                            })
                                    )
                                );
                            }

                            return true;
                        }
                    } else if (
                        pipe.id &&
                        (pipe.slot instanceof Slots.String ||
                            pipe.slot instanceof Slots.Text)
                    ) {
                        addScore(identifier, pipe);

                        return true;
                    }

                    return false;
                });
            } else if (
                identifier.id &&
                (identifier.slot instanceof Slots.String ||
                    identifier.slot instanceof Slots.Text)
            ) {
                addScore(identifier, identifier);
            }
        }
    );

    return menu;
};

const compareMenu = (calculator: ICalculator, operator: TOperators) => {
    const menu: Components.MenuOption[] = [];
    let cluster: string | undefined;
    const addCluster = (src: Cluster | undefined) => {
        if (src?.id !== cluster) {
            const name = src?.name;

            menu.push(new Components.MenuSeparator());

            if (name || menu.length === 1) {
                menu.push(
                    new Components.MenuLabel(
                        name || pgettext("block:calculator", "Blocks")
                    )
                );
            }

            cluster = src?.id;
        }
    };

    each(
        populateSlots(calculator.block, {
            mode: "validated",
            pipes: "exclude",
        }),
        (identifier) => {
            if (identifier.slots) {
                const children: Components.MenuOption[] = [];
                let fnAction: (() => void) | undefined;

                each(identifier.slots, (slot) => {
                    if (
                        slot.id &&
                        (slot.slot instanceof Slots.String ||
                            slot.slot instanceof Slots.Text ||
                            slot.slot instanceof Slots.Boolean ||
                            slot.slot instanceof Slots.Number ||
                            slot.slot instanceof Slots.Numeric ||
                            slot.slot instanceof Slots.Date)
                    ) {
                        const slotSupply =
                            (slot.slot instanceof Slots.String ||
                                slot.slot instanceof Slots.Text) &&
                            Collection.find(slot);

                        if (!slotSupply || !slotSupply.sole) {
                            children.push(
                                new Components.MenuItemWithIcon(
                                    getSlotIcon(slot.slot),
                                    slot.label,
                                    (fnAction = () =>
                                        calculator.operations.append().set(
                                            slot.slot instanceof Slots.String ||
                                                slot.slot instanceof Slots.Text
                                                ? {
                                                      operator,
                                                      opcode: "evaluate",
                                                      value: slot.id,
                                                      cona: 0,
                                                      conb: 0,
                                                      outa: 1,
                                                      outb: 0,
                                                      open: true,
                                                  }
                                                : slot.slot instanceof
                                                  Slots.Boolean
                                                ? {
                                                      operator,
                                                      opcode: "boolean",
                                                      value: slot.id,
                                                      outa: 1,
                                                      outb: 0,
                                                      open: true,
                                                  }
                                                : slot.slot instanceof
                                                  Slots.Date
                                                ? {
                                                      operator,
                                                      opcode: "date",
                                                      value: slot.id,
                                                      cona: undefined,
                                                      conb: undefined,
                                                      outa: 1,
                                                      outb: 0,
                                                      open: true,
                                                  }
                                                : {
                                                      operator,
                                                      opcode: "equation",
                                                      value: slot.id,
                                                      cona: 0,
                                                      conb: 0,
                                                      outa: 1,
                                                      outb: 0,
                                                      open: true,
                                                  }
                                        ))
                                )
                            );
                        }
                    }
                });

                if (children.length > 0) {
                    addCluster(identifier.block.node.cluster);

                    menu.push(
                        children.length === 1 && fnAction
                            ? new Components.MenuItemWithImage(
                                  identifier.icon || identifier.block.type.icon,
                                  identifier.label,
                                  fnAction
                              )
                            : new Components.MenuSubmenuWithImage(
                                  identifier.icon || identifier.block.type.icon,
                                  identifier.label,
                                  children
                              )
                    );
                }
            } else if (
                identifier.id &&
                (identifier.slot instanceof Slots.String ||
                    identifier.slot instanceof Slots.Text ||
                    identifier.slot instanceof Slots.Boolean ||
                    identifier.slot instanceof Slots.Number ||
                    identifier.slot instanceof Slots.Numeric ||
                    identifier.slot instanceof Slots.Date)
            ) {
                const slotSupply =
                    (identifier.slot instanceof Slots.String ||
                        identifier.slot instanceof Slots.Text) &&
                    Collection.find(identifier);

                if (!slotSupply || !slotSupply.sole) {
                    addCluster(identifier.block.node.cluster);

                    menu.push(
                        new Components.MenuItemWithImage(
                            identifier.icon || identifier.block.type.icon,
                            identifier.label,
                            () =>
                                calculator.operations.append().set(
                                    identifier.slot instanceof Slots.String ||
                                        identifier.slot instanceof Slots.Text
                                        ? {
                                              operator,
                                              opcode: "evaluate",
                                              value: identifier.id,
                                              cona: 0,
                                              conb: 0,
                                              outa: 1,
                                              outb: 0,
                                              open: true,
                                          }
                                        : identifier.slot instanceof
                                          Slots.Boolean
                                        ? {
                                              operator,
                                              opcode: "boolean",
                                              value: identifier.id,
                                              outa: 1,
                                              outb: 0,
                                              open: true,
                                          }
                                        : identifier.slot instanceof Slots.Date
                                        ? {
                                              operator,
                                              opcode: "date",
                                              value: identifier.id,
                                              cona: undefined,
                                              conb: undefined,
                                              outa: 1,
                                              outb: 0,
                                              open: true,
                                          }
                                        : {
                                              operator,
                                              opcode: "equation",
                                              value: identifier.id,
                                              cona: 0,
                                              conb: 0,
                                              outa: 1,
                                              outb: 0,
                                              open: true,
                                          }
                                )
                        )
                    );
                }
            }
        }
    );

    return menu;
};

const functionsMenu = (
    operator: TOperators,
    operations: Collection.Provider<Operation, ICalculator>,
    isNumber: boolean,
    variable?: string,
    constants: boolean = true
) => [
    new Components.MenuSubmenuWithIcon(
        0xe6f2,
        pgettext("block:calculator", "Limiting"),
        [
            new Components.MenuItem("min", () =>
                operations.append().set({
                    operator,
                    opcode: "min",
                    value: isNumber ? 0 : variable,
                    min: 0,
                    open: true,
                })
            ),
            new Components.MenuItem("max", () =>
                operations.append().set({
                    operator,
                    opcode: "max",
                    value: isNumber ? 0 : variable,
                    max: 0,
                    open: true,
                })
            ),
            new Components.MenuItem("clamp", () =>
                operations.append().set({
                    operator,
                    opcode: "clamp",
                    value: isNumber ? 0 : variable,
                    min: 0,
                    max: 0,
                    open: true,
                })
            ),
        ]
    ),
    new Components.MenuSubmenuWithImage(
        ICON_NUMBER,
        pgettext("block:calculator", "Floating point"),
        [
            new Components.MenuItem("round", () =>
                operations.append().set({
                    operator,
                    opcode: "round",
                    value: isNumber ? 0 : variable,
                    open: isNumber,
                })
            ),
            new Components.MenuItem("floor", () =>
                operations.append().set({
                    operator,
                    opcode: "floor",
                    value: isNumber ? 0 : variable,
                    open: isNumber,
                })
            ),
            new Components.MenuItem("ceil", () =>
                operations.append().set({
                    operator,
                    opcode: "ceil",
                    value: isNumber ? 0 : variable,
                    open: isNumber,
                })
            ),
            new Components.MenuItem("trunc", () =>
                operations.append().set({
                    operator,
                    opcode: "trunc",
                    value: isNumber ? 0 : variable,
                    open: isNumber,
                })
            ),
        ]
    ),
    new Components.MenuSubmenuWithImage(
        ICON_EXPONENTIATION,
        pgettext("block:calculator", "Exponentiation"),
        [
            new Components.MenuItem("x²", () =>
                operations.append().set({
                    operator,
                    opcode: "square",
                    value: isNumber ? 0 : variable,
                    open: isNumber,
                })
            ),
            new Components.MenuItem("√", () =>
                operations.append().set({
                    operator,
                    opcode: "sqrt",
                    value: isNumber ? 0 : variable,
                    open: isNumber,
                })
            ),
            new Components.MenuItem("pow", () =>
                operations.append().set({
                    operator,
                    opcode: "pow",
                    exponent: 1,
                    open: true,
                })
            ),
            new Components.MenuItem("exp", () =>
                operations.append().set({
                    operator,
                    opcode: "exp",
                    exponent: isNumber ? 0 : undefined,
                    open: isNumber,
                })
            ),
            new Components.MenuItem("ln", () =>
                operations.append().set({
                    operator,
                    opcode: "ln",
                    value: isNumber ? 0 : variable,
                    open: isNumber,
                })
            ),
            new Components.MenuItem("log", () =>
                operations.append().set({
                    operator,
                    opcode: "log",
                    value: isNumber ? 0 : variable,
                    open: isNumber,
                })
            ),
        ]
    ),
    new Components.MenuSubmenuWithIcon(
        0xe982,
        pgettext("block:calculator", "Trigonometry"),
        [
            new Components.MenuItem("sin", () =>
                operations.append().set({
                    operator,
                    opcode: "sin",
                    value: isNumber ? 0 : variable,
                    open: isNumber,
                })
            ),
            new Components.MenuItem("cos", () =>
                operations.append().set({
                    operator,
                    opcode: "cos",
                    value: isNumber ? 0 : variable,
                    open: isNumber,
                })
            ),
            new Components.MenuItem("tan", () =>
                operations.append().set({
                    operator,
                    opcode: "tan",
                    value: isNumber ? 0 : variable,
                    open: isNumber,
                })
            ),
            new Components.MenuSeparator(),
            new Components.MenuLabel(pgettext("block:calculator", "Inverse")),
            new Components.MenuItem("sin⁻¹", () =>
                operations.append().set({
                    operator,
                    opcode: "asin",
                    value: isNumber ? 0 : variable,
                    open: isNumber,
                })
            ),
            new Components.MenuItem("cos⁻¹", () =>
                operations.append().set({
                    operator,
                    opcode: "acos",
                    value: isNumber ? 0 : variable,
                    open: isNumber,
                })
            ),
            new Components.MenuItem("tan⁻¹", () =>
                operations.append().set({
                    operator,
                    opcode: "atan",
                    value: isNumber ? 0 : variable,
                    open: isNumber,
                })
            ),
            new Components.MenuSeparator(),
            new Components.MenuLabel(
                pgettext("block:calculator", "Hyperbolic")
            ),
            new Components.MenuItem("sinh", () =>
                operations.append().set({
                    operator,
                    opcode: "sinh",
                    value: isNumber ? 0 : variable,
                    open: isNumber,
                })
            ),
            new Components.MenuItem("cosh", () =>
                operations.append().set({
                    operator,
                    opcode: "cosh",
                    value: isNumber ? 0 : variable,
                    open: isNumber,
                })
            ),
            new Components.MenuItem("tanh", () =>
                operations.append().set({
                    operator,
                    opcode: "tanh",
                    value: isNumber ? 0 : variable,
                    open: isNumber,
                })
            ),
            new Components.MenuSeparator(),
            new Components.MenuLabel(
                pgettext("block:calculator", "Inverse hyperbolic")
            ),
            new Components.MenuItem("sinh⁻¹", () =>
                operations.append().set({
                    operator,
                    opcode: "asinh",
                    value: isNumber ? 0 : variable,
                    open: isNumber,
                })
            ),
            new Components.MenuItem("cosh⁻¹", () =>
                operations.append().set({
                    operator,
                    opcode: "acosh",
                    value: isNumber ? 0 : variable,
                    open: isNumber,
                })
            ),
            new Components.MenuItem("tanh⁻¹", () =>
                operations.append().set({
                    operator,
                    opcode: "atanh",
                    value: isNumber ? 0 : variable,
                    open: isNumber,
                })
            ),
        ]
    ),
    new Components.MenuSubmenuWithIcon(
        0xe954,
        pgettext("block:calculator", "Factorial"),
        [
            new Components.MenuItem("n!", () =>
                operations.append().set({
                    operator,
                    opcode: "fact",
                    value: isNumber ? 0 : variable,
                    open: isNumber,
                })
            ),
            new Components.MenuItem("gamma", () =>
                operations.append().set({
                    operator,
                    opcode: "gamma",
                    value: isNumber ? 0 : variable,
                    open: isNumber,
                })
            ),
        ]
    ),
    new Components.MenuSubmenuWithIcon(
        0xe938,
        pgettext("block:calculator", "Miscellaneous"),
        [
            new Components.MenuItem("abs", () =>
                operations.append().set({
                    operator,
                    opcode: "abs",
                    value: isNumber ? 0 : variable,
                    open: isNumber,
                })
            ),
            new Components.MenuItem("sgn", () =>
                operations.append().set({
                    operator,
                    opcode: "sgn",
                    value: isNumber ? 0 : variable,
                    open: isNumber,
                })
            ),
            new Components.MenuItem("mod", () =>
                operations.append().set({
                    operator,
                    opcode: "mod",
                    value: isNumber ? 0 : variable,
                    divisor: 1,
                    open: true,
                })
            ),
            new Components.MenuItem("%", () =>
                operations.append().set({
                    operator,
                    opcode: "percentage",
                    value: isNumber ? 0 : variable,
                    percentage: 0,
                    open: true,
                })
            ),
        ]
    ),
    ...(constants
        ? [
              new Components.MenuSubmenuWithIcon(
                  0xe756,
                  pgettext("block:calculator", "Constants"),
                  () =>
                      CONSTANTS.map(
                          (value) =>
                              new Components.MenuItemWithIcon(
                                  getConstantIcon(value),
                                  getConstantLabel(value),
                                  () =>
                                      operations.append().set({
                                          operator,
                                          opcode: "number",
                                          value,
                                      })
                              )
                      )
              ),
          ]
        : []),
];

const blocksMenu = (calculator: ICalculator, operator: TOperators) => {
    const menu: Components.MenuOption[] = [];
    let cluster: string | undefined;
    const addCluster = (src: Cluster | undefined) => {
        if (src?.id !== cluster) {
            const name = src?.name;

            if (menu.length > 0 || name) {
                menu.push(new Components.MenuSeparator());
            }

            if (name) {
                menu.push(new Components.MenuLabel(name));
            }

            cluster = src?.id;
        }
    };

    each(
        populateSlots(calculator.block, {
            mode: "validated",
            pipes: "include-consumed-slots",
        }),
        (identifier) => {
            if (identifier.slots) {
                const children: Components.MenuOption[] = [];
                let isFirst = true;
                let hasScore = false;
                let hasCounter = false;

                findFirst(identifier.slots, (pipe) => {
                    if (pipe.type === "pipe") {
                        const supply = Collection.find(pipe);

                        if (supply) {
                            const scoreSlot = pipe.slot?.slots?.select(
                                "score",
                                "feature"
                            );

                            hasScore = true;

                            if (scoreSlot) {
                                const scores: Components.MenuOption[] = [];

                                addSlot(
                                    scores,
                                    operator,
                                    calculator.operations,
                                    {
                                        type: "slot",
                                        block: identifier.block,
                                        label:
                                            scoreSlot.alias ||
                                            pgettext(
                                                "block:calculator",
                                                "Existing score"
                                            ),
                                        icon:
                                            identifier.icon ||
                                            identifier.block.type.icon,
                                        id: scoreSlot.id,
                                        reference: scoreSlot.reference,
                                        slot: scoreSlot,
                                    },
                                    false,
                                    true
                                );

                                scores.push(
                                    new Components.MenuItemWithIcon(
                                        0xe70b,
                                        pgettext(
                                            "block:calculator",
                                            "New score"
                                        ),
                                        () =>
                                            calculator.operations.append().set({
                                                operator,
                                                opcode: "score",
                                                value: pipe.id,
                                                open: true,
                                            })
                                    )
                                );

                                children.push(
                                    new Components.MenuSubmenuWithIcon(
                                        0xe6e1,
                                        pgettext("block:calculator", "Score"),
                                        scores
                                    )
                                );
                            } else {
                                children.push(
                                    new Components.MenuItemWithIcon(
                                        0xe6e1,
                                        pgettext("block:calculator", "Score"),
                                        () =>
                                            calculator.operations.append().set({
                                                operator,
                                                opcode: "score",
                                                value: pipe.id,
                                                open: true,
                                            })
                                    )
                                );
                            }

                            if (supply.origin && supply.sole) {
                                if (
                                    pipe.slot instanceof Slots.Number ||
                                    pipe.slot instanceof Slots.Numeric
                                ) {
                                    children.push(
                                        new Components.MenuItemWithIcon(
                                            0xe9bd,
                                            pgettext("block:calculator", "Sum"),
                                            () =>
                                                calculator.operations
                                                    .append()
                                                    .set({
                                                        operator,
                                                        opcode: "sum",
                                                        value: pipe.id,
                                                    })
                                        )
                                    );
                                } else if (pipe.slot instanceof Slots.Boolean) {
                                    const counterSlot =
                                        pipe.slot.slots?.select("counter");

                                    if (counterSlot) {
                                        hasCounter = true;

                                        addSlot(
                                            children,
                                            operator,
                                            calculator.operations,
                                            {
                                                type: "slot",
                                                block: identifier.block,
                                                label:
                                                    counterSlot.label ||
                                                    pgettext(
                                                        "block:calculator",
                                                        "Counter"
                                                    ),
                                                icon: ICON_COUNTER,
                                                id: counterSlot.id,
                                                reference:
                                                    counterSlot.reference,
                                                slot: counterSlot,
                                            },
                                            false,
                                            true
                                        );
                                    } else {
                                        children.push(
                                            new Components.MenuItemWithIcon(
                                                0xe92e,
                                                pgettext(
                                                    "block:calculator",
                                                    "Count"
                                                ),
                                                () =>
                                                    calculator.operations
                                                        .append()
                                                        .set({
                                                            operator,
                                                            opcode: "count",
                                                            value: pipe.id,
                                                        })
                                            )
                                        );
                                    }
                                }
                            }

                            return true;
                        }
                    }

                    return false;
                });

                each(identifier.slots, (slot) => {
                    if (
                        slot.type === "slot" &&
                        slot.id &&
                        slot.slot &&
                        ((slot.reference !== "score" &&
                            slot.reference !== "counter") ||
                            (slot.reference === "score" && !hasScore) ||
                            (slot.reference === "counter" && !hasCounter))
                    ) {
                        if (isFirst) {
                            isFirst = false;

                            if (children.length > 0) {
                                children.push(new Components.MenuSeparator());
                            }
                        }

                        addSlot(
                            children,
                            operator,
                            calculator.operations,
                            slot,
                            true
                        );
                    }
                });

                if (children.length > 0) {
                    addCluster(identifier.block.node.cluster);

                    menu.push(
                        new Components.MenuSubmenuWithImage(
                            identifier.icon || identifier.block.type.icon,
                            identifier.label,
                            children
                        )
                    );
                }
            } else if (identifier.id && identifier.slot) {
                const children: Components.MenuOption[] = [];

                addSlot(
                    children,
                    operator,
                    calculator.operations,
                    identifier,
                    false
                );

                if (children.length > 0) {
                    addCluster(identifier.block.node.cluster);

                    menu.push(...children);
                }
            }
        }
    );

    return menu;
};

const addSlot = (
    menu: Components.MenuOption[],
    operator: TOperators,
    operations: Collection.Provider<Operation, ICalculator>,
    identifier: ISlotIdentifier,
    isSubmenu: boolean,
    allowScore: boolean = false
) => {
    if (
        identifier.slot instanceof Slots.String ||
        identifier.slot instanceof Slots.Text
    ) {
        const scoreSupply = Collection.find(identifier);
        const metadata = getMetadata<ICalculatorMetadata>(
            identifier.block,
            "calculator"
        );
        const fnScore = () =>
            operations.append().set({
                operator,
                opcode: "score",
                value: identifier.id,
                open: true,
            });
        const allowDefault =
            (scoreSupply?.sole &&
                metadata &&
                (metadata[identifier.slot.reference] || metadata["*"])
                    ?.allowDefault === true) ||
            (!scoreSupply?.sole &&
                (!metadata ||
                    (metadata[identifier.slot.reference] || metadata["*"])
                        ?.allowDefault !== false));
        const allowCastToNumber =
            (scoreSupply?.sole &&
                metadata &&
                (metadata[identifier.slot.reference] || metadata["*"])
                    ?.allowCastToNumber === true) ||
            (!scoreSupply?.sole &&
                (!metadata ||
                    (metadata[identifier.slot.reference] || metadata["*"])
                        ?.allowCastToNumber !== false));
        const scoreSlot = identifier.slot.slots?.select("score", "feature");
        const allowScores =
            scoreSupply ||
            (metadata &&
                (metadata[identifier.slot.reference] || metadata["*"])?.scores)
                ? true
                : false;

        if (
            (scoreSupply?.sole && !allowCastToNumber) ||
            (metadata &&
                (metadata[identifier.slot.reference] || metadata["*"])
                    ?.scores &&
                !allowDefault &&
                !allowCastToNumber)
        ) {
            if (scoreSlot) {
                const children: Components.MenuOption[] = [];

                addSlot(
                    children,
                    operator,
                    operations,
                    {
                        type: "slot",
                        block: identifier.block,
                        label:
                            scoreSlot.alias ||
                            pgettext("block:calculator", "Existing score"),
                        icon: identifier.icon || identifier.block.type.icon,
                        id: scoreSlot.id,
                        reference: scoreSlot.reference,
                        slot: scoreSlot,
                    },
                    false,
                    true
                );

                children.push(
                    new Components.MenuItemWithIcon(
                        0xe70b,
                        pgettext("block:calculator", "New score"),
                        () =>
                            operations.append().set({
                                operator,
                                opcode: "score",
                                value: identifier.id,
                                open: true,
                            })
                    )
                );

                menu.push(
                    isSubmenu
                        ? new Components.MenuSubmenuWithIcon(
                              0xe6e1,
                              pgettext("block:calculator", "Score"),
                              children
                          )
                        : new Components.MenuSubmenuWithImage(
                              identifier.icon || identifier.block.type.icon,
                              identifier.label,
                              children
                          )
                );
            } else {
                menu.push(
                    isSubmenu
                        ? new Components.MenuItemWithIcon(
                              0xe6e1,
                              identifier.label,
                              fnScore
                          )
                        : new Components.MenuItemWithImage(
                              identifier.icon || identifier.block.type.icon,
                              identifier.label,
                              fnScore
                          )
                );
            }
        } else {
            const options = [];

            if (allowScores || scoreSlot) {
                if (scoreSlot) {
                    const children: Components.MenuOption[] = [];

                    addSlot(
                        children,
                        operator,
                        operations,
                        {
                            type: "slot",
                            block: identifier.block,
                            label:
                                scoreSlot.alias ||
                                pgettext("block:calculator", "Existing score"),
                            icon: identifier.icon || identifier.block.type.icon,
                            id: scoreSlot.id,
                            reference: scoreSlot.reference,
                            slot: scoreSlot,
                        },
                        false,
                        true
                    );

                    children.push(
                        new Components.MenuItemWithIcon(
                            0xe70b,
                            pgettext("block:calculator", "New score"),
                            fnScore
                        )
                    );

                    options.push(
                        new Components.MenuSubmenuWithIcon(
                            0xe6e1,
                            pgettext("block:calculator", "Score"),
                            children
                        )
                    );
                } else {
                    options.push(
                        new Components.MenuItemWithIcon(
                            0xe6e1,
                            pgettext("block:calculator", "Score"),
                            fnScore
                        )
                    );
                }
            }

            if (allowDefault) {
                options.push(
                    new Components.MenuItemWithIcon(
                        0xe839,
                        pgettext("block:calculator", "Compare"),
                        () =>
                            operations.append().set({
                                operator,
                                opcode: "evaluate",
                                value: identifier.id,
                                cona: 0,
                                conb: 0,
                                outa: 1,
                                outb: 0,
                                open: true,
                            })
                    ),
                    new Components.MenuItemWithIcon(
                        0xe97b,
                        pgettext("block:calculator", "Character count"),
                        () =>
                            operations.append().set({
                                operator,
                                opcode: "chars",
                                value: identifier.id,
                            })
                    ),
                    new Components.MenuItemWithIcon(
                        0xe97b,
                        pgettext("block:calculator", "Word count"),
                        () =>
                            operations.append().set({
                                operator,
                                opcode: "words",
                                value: identifier.id,
                            })
                    ),
                    new Components.MenuItemWithIcon(
                        0xe97b,
                        pgettext("block:calculator", "Line count"),
                        () =>
                            operations.append().set({
                                operator,
                                opcode: "lines",
                                value: identifier.id,
                            })
                    ),
                    new Components.MenuItemWithIcon(
                        0xe923,
                        pgettext("block:calculator", "Count occurrences"),
                        () =>
                            operations.append().set({
                                operator,
                                opcode: "occurrences",
                                value: identifier.id,
                                open: true,
                            })
                    )
                );
            }

            if (allowCastToNumber) {
                options.push(
                    new Components.MenuItemWithIcon(
                        0xe6ed,
                        pgettext("block:calculator", "Convert to number"),
                        () =>
                            operations.append().set({
                                operator,
                                opcode: "cast",
                                value: identifier.id,
                            })
                    )
                );
            }

            if (options.length > 0) {
                menu.push(
                    options.length === 1
                        ? options[0]
                        : new Components.MenuSubmenuWithImage(
                              identifier.icon || identifier.block.type.icon,
                              identifier.label,
                              options
                          )
                );
            }
        }
    } else if (
        (identifier.slot instanceof Slots.Number ||
            identifier.slot instanceof Slots.Numeric) &&
        (allowScore ||
            identifier.reference !== "score" ||
            !hasScoreSupply(identifier))
    ) {
        const reference =
            identifier.block instanceof Calculator
                ? "calculator"
                : identifier.slot.reference;
        const options = () => [
            new Components.MenuItemWithIcon(
                reference === "calculator"
                    ? 0xe766
                    : reference === "score"
                    ? 0xe6e1
                    : reference === "counter"
                    ? 0xe92e
                    : 0xe934,
                reference === "calculator"
                    ? pgettext("block:calculator", "Calculation outcome")
                    : reference === "score"
                    ? pgettext("block:calculator", "Score value")
                    : reference === "counter"
                    ? pgettext("block:calculator", "Counter value")
                    : pgettext("block:calculator", "Current value"),
                () =>
                    operations.append().set({
                        operator,
                        opcode: "number",
                        value: identifier.id,
                    })
            ),
            new Components.MenuItemWithIcon(
                0xe839,
                pgettext("block:calculator", "Compare"),
                () =>
                    operations.append().set({
                        operator,
                        opcode: "equation",
                        value: identifier.id,
                        cona: 0,
                        conb: 0,
                        outa: 1,
                        outb: 0,
                        open: true,
                    })
            ),
            new Components.MenuSeparator(),
            new Components.MenuLabel(pgettext("block:calculator", "Functions")),
            ...functionsMenu(operator, operations, false, identifier.id, false),
        ];

        menu.push(
            !(identifier.block instanceof Calculator) &&
                (identifier.reference === "calculator" ||
                    identifier.reference === "score")
                ? new Components.MenuSubmenuWithIcon(
                      identifier.reference === "calculator" ? 0xe766 : 0xe6e1,
                      identifier.label,
                      options
                  )
                : new Components.MenuSubmenuWithImage(
                      identifier.icon || identifier.block.type.icon,
                      identifier.label,
                      options
                  )
        );
    } else if (identifier.slot instanceof Slots.Boolean) {
        const fnOperation = () =>
            operations.append().set({
                operator,
                opcode: "boolean",
                value: identifier.id,
                outa: 1,
                outb: 0,
                open: true,
            });

        menu.push(
            isSubmenu
                ? new Components.MenuItemWithIcon(
                      0xe999,
                      identifier.label,
                      fnOperation
                  )
                : new Components.MenuItemWithImage(
                      identifier.icon || identifier.block.type.icon,
                      identifier.label,
                      fnOperation
                  )
        );
    } else if (identifier.slot instanceof Slots.Date) {
        menu.push(
            new Components.MenuSubmenuWithImage(
                identifier.icon || identifier.block.type.icon,
                identifier.label,
                [
                    new Components.MenuItemWithIcon(
                        0xe839,
                        pgettext("block:calculator", "Compare"),
                        () =>
                            operations.append().set({
                                operator,
                                opcode: "date",
                                value: identifier.id,
                                cona: undefined,
                                conb: undefined,
                                outa: 1,
                                outb: 0,
                                open: true,
                            })
                    ),
                    new Components.MenuItemWithIcon(
                        0xe7fe,
                        pgettext("block:calculator", "Age"),
                        () =>
                            operations.append().set({
                                operator,
                                opcode: "age",
                                value: identifier.id,
                                ageAbs: true,
                            })
                    ),
                    new Components.MenuSeparator(),
                    new Components.MenuLabel(
                        pgettext("block:calculator", "Date related")
                    ),
                    new Components.MenuItemWithIcon(
                        0xe785,
                        pgettext("block:calculator", "Year"),
                        () =>
                            operations.append().set({
                                operator,
                                opcode: "year",
                                value: identifier.id,
                            })
                    ),
                    new Components.MenuItemWithIcon(
                        0xe789,
                        pgettext("block:calculator", "Month (January = 1)"),
                        () =>
                            operations.append().set({
                                operator,
                                opcode: "month",
                                value: identifier.id,
                            })
                    ),
                    new Components.MenuItemWithIcon(
                        0xe788,
                        pgettext("block:calculator", "Day of month (1-31)"),
                        () =>
                            operations.append().set({
                                operator,
                                opcode: "day-of-month",
                                value: identifier.id,
                            })
                    ),
                    new Components.MenuItemWithIcon(
                        0xe78a,
                        pgettext(
                            "block:calculator",
                            "Day of week (Sunday = 0)"
                        ),
                        () =>
                            operations.append().set({
                                operator,
                                opcode: "day-of-week",
                                value: identifier.id,
                            })
                    ),
                    ...(identifier.slot.supportsTime
                        ? [
                              new Components.MenuSeparator(),
                              new Components.MenuLabel(
                                  pgettext("block:calculator", "Time related")
                              ),
                              new Components.MenuItemWithIcon(
                                  0xe8e8,
                                  pgettext("block:calculator", "Hour (0-23)"),
                                  () =>
                                      operations.append().set({
                                          operator,
                                          opcode: "hour",
                                          value: identifier.id,
                                      })
                              ),
                              new Components.MenuItemWithIcon(
                                  0xe8e8,
                                  pgettext("block:calculator", "Minute (0-59)"),
                                  () =>
                                      operations.append().set({
                                          operator,
                                          opcode: "minute",
                                          value: identifier.id,
                                      })
                              ),
                              new Components.MenuItemWithIcon(
                                  0xe8e8,
                                  pgettext("block:calculator", "Second (0-59)"),
                                  () =>
                                      operations.append().set({
                                          operator,
                                          opcode: "second",
                                          value: identifier.id,
                                      })
                              ),
                              new Components.MenuItemWithIcon(
                                  0xe8e8,
                                  pgettext(
                                      "block:calculator",
                                      "Millisecond (0-999)"
                                  ),
                                  () =>
                                      operations.append().set({
                                          operator,
                                          opcode: "millisecond",
                                          value: identifier.id,
                                      })
                              ),
                          ]
                        : []),
                ]
            )
        );
    }
};

const hasScoreSupply = (identifier: ISlotIdentifier) => {
    return identifier.slot?.slots?.each((slot) => {
        if (slot instanceof Slots.String || slot instanceof Slots.Text) {
            const metadata = getMetadata<ICalculatorMetadata>(
                identifier.block,
                "calculator"
            );

            return Collection.find({
                ...identifier,
                slot,
            }) ||
                (metadata &&
                    (metadata[slot.reference] || metadata["*"])?.scores)
                ? true
                : false;
        }

        return false;
    });
};
