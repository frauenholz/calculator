import { Calculator as RunnerCalculator } from "../runner/esm/index.mjs";
import { Calculator as BuilderCalculator } from "../builder/esm/index.mjs";

try {
    if (typeof RunnerCalculator === "undefined") {
        throw new Error();
    }

    if (typeof BuilderCalculator === "undefined") {
        throw new Error();
    }
} catch(e) {
    throw new Error("ESM module failed!");
}
